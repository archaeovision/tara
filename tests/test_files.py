import unittest
import json

from cStringIO import StringIO

from tara.api import controller as api


UNITTEST_FILENAME_1 = json.dumps({'filename': 'test1.txt'})
UNITTEST_FILENAME_2 = json.dumps({'filename': 'test2.txt'})

UNITTEST_FILE = dict(upload_var=(StringIO("hi everyone"), UNITTEST_FILENAME_1))


class FileTestCase(unittest.TestCase):
    def setUp(self):
        app = api.create_app(TESTING=True)
        self.client = app.test_client()

        rv = self.client.get('/files/')
        self.assertEqual(rv.status_code, 200)

    def delete_file(self, _id=None):
        if _id:
            rv = self.client.delete('/files/' + _id)
            self.assertEqual(rv.status_code, 200)

    def create_file(self, cleanup=True):
        rv = self.client.post('/files/', data=dict(upload_var=(StringIO("hi everyone"), {'filename': 'test1.txt'})))

        # verify result
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        _id = json.loads(rv.data)['_id']
        self.assertEqual(len(_id), 24)

        if cleanup:
            self.delete_file(_id)

        return _id

    def update_filename(self):
        # create file
        _id = self.create_file(cleanup=False)

        # update file
        rv = self.client.patch('/files/'+_id, data=UNITTEST_FILENAME_2, content_type='application/json')

        # verify result
        self.assertEqual(rv.status_code, 200, str(rv.data))
        self.assertEqual(rv.content_type, 'application/json')

        # delete file
        rv = self.client.delete('/files/' + _id)
        self.assertEqual(rv.status_code, 200)

        # delete non-existing file
        rv = self.client.delete('/files/' + _id)
        self.assertEqual(rv.status_code, 404)

        # try retrive deleted file
        rv = self.client.get('/files/'+_id)
        self.assertEqual(rv.status_code, 404)

    def download_file(self):
        # create file
        _id = self.create_file(cleanup=False)

        # download file
        rv = self.client.get('/download/'+_id)
        self.assertEqual(rv.status_code, 200)

        # delete file
        rv = self.client.delete('/files/' + _id)
        self.assertEqual(rv.status_code, 200)
        print rv.content_type
