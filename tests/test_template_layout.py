import unittest
import json

from tara.api import controller as api

UNITTEST_TEMPLATE_LAYOUT = json.dumps({
    'label': '_unittesttempaltelayout_',
    'sort': 0,
    'widgets': [
        {
            'path': 'path/to/widget',
            'config': {
                'key1': 'value1'
            }
        }
    ]
})


class TemplateLayoutTestCase(unittest.TestCase):
    def setUp(self):
        app = api.create_app(TESTING=True)
        self.client = app.test_client()

    def delete_template_layout(self, _id=None):
        if _id:
            rv = self.client.delete('/templates/layouts/' + _id)
            self.assertEqual(rv.status_code, 200)
            self.assertEqual(rv.content_type, 'application/json')

    def create_template_layout(self, cleanup=True):
        # create template layout
        rv = self.client.post('/templates/layout/', data=UNITTEST_TEMPLATE_LAYOUT, content_type='application/json')

        # verify response
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get template id
        _id = json.loads(rv.data)['_id']
        self.assertEqual(len(_id), 24)

        if cleanup:
            self.test_delete_template(_id)

        return _id

    def update_template_layout(self):
        # create template layout
        _id = self.create_template_layout(cleanup=False)

        # update template layout
        rv = self.client.patch('/templates/'+_id, data=UNITTEST_TEMPLATE_LAYOUT, content_type='application/json')

        # verify response
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # delete template
        rv = self.client.delete('/templates/layout/' + _id)

        # verify response
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(rv.content_type, 'application/json')

        # delete non-existing template
        rv = self.client.delete('/templates/layout/' + _id)

        # verify response
        self.assertEqual(rv.status_code, 404)
        self.assertEqual(rv.content_type, 'application/json')

        # try retrive deleted template
        rv = self.client.get('/templates/'+_id)

        # verify response
        self.assertEqual(rv.status_code, 404)
        self.assertEqual(rv.content_type, 'application/json')
