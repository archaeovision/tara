import os.path
from tara import factory
from functools import wraps
from flask import request, redirect, current_app, render_template


def ssl_required(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if current_app.config.get("SSL"):
            if request.is_secure:
                return fn(*args, **kwargs)
            else:
                return redirect(request.url.replace("http://", "https://"))
        return fn(*args, **kwargs)
    return decorated_view


def create_app(settings_override=None):
    """Returns the API application instance"""
    app = factory.create_app(__name__, settings_override)

    @app.route('/not_supported')
    def not_supported():
        return render_template('not_supported.html')

    @app.route('/login/id_callback', methods=['POST'])
    def id_callback():
        user = request.form.get('user')
        grant = request.form.get('grant')
        return render_template('id_auth.html', user=user, grant=grant)

    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>')
    def index(path):
        output= '''<!DOCTYPE html>
<html>
<head>'''
        output=output+'<title>'+app.config['HTML_TITLE']+'</title>'+"\n"
        if app.config['FAVICON_URI']:
            output=output+'<link rel="shortcut icon" href="'+app.config['FAVICON_URI']+'" />'+"\n"
        
        for style in app.config['LOAD_STYLESHEETS']:
            
            if "http" in style:
                link=style
            elif os.path.isfile(app.config['ROOT_DIRECTORY']+'/tara/frontend/'+app.config['THEME_URI']+style):
                link=app.config['THEME_URI']+style
            elif os.path.isfile(app.config['ROOT_DIRECTORY']+'/tara/frontend/static/css/'+style):
                link='/static/css/'+style
            else:
                link=style
            output=output+'<link rel="stylesheet" href="'+link+'">'+"\n"

        output=output+'''<script data-main="/static/js/main" src="/static/js/libs/require/require.js"></script>
<meta charset="utf-8">
</head>
<body>
</body>
</html>
'''
        return output
    return app
