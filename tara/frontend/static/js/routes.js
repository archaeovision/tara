define([
  'backbone',
  'layouts/base',
  'views/category/add',
  'views/category/edit',
  'views/category/list',
  'views/classificator/add',
  'views/classificator/edit',
  'views/classificator/list',
  'views/file/add',
  'views/file/add_existing',
  'views/file/edit',
  'views/file/list',
  'views/frontpage/main',
  'views/import/main',
  'views/login/login',
  'views/login/logout',
  'views/resource/add',
  'views/resource/edit',
  'views/resource/list',
  'views/resource/geoview',
  'views/settings/main',
  'views/template/add',
  'views/template/edit',
  'views/template/list',
  'views/template/layout/add',
  'views/template/layout/edit',
  'views/template/layout/list',
  'views/user/add',
  'views/user/change_password',
  'views/user/edit',
  'views/user/list',
  'views/revision/list',
  'views/role/add',
  'views/role/edit',
  'views/role/list',
  'collections/template',
  'backbone-queryparams',
  'bootstrap/bootstrap-alert',
  'bootstrap/bootstrap-collapse',
  'bootstrap/bootstrap-dropdown',
  'bootstrap/bootstrap-typeahead',
  'bootstrap/bootstrap-tooltip',
  'bootstrap/bootstrap-popover'
], function (Backbone, BaseLayout, CategoryAddView, CategoryEditView, CategoryListView, ClassificatorAddView, ClassificatorEditView, ClassificatorListView, FileAddView, FileAddExistingView, FileEditView, FileListView, FrontPageView, ImportView, LoginView, LogoutView, ResourceAddView, ResourceEditView, ResourceListView, ResourceGeoView, SettingsView, TemplateAddView, TemplateEditView, TemplateListView, TemplateLayoutAddView, TemplateLayoutEditView, TemplateLayoutListView, UserAddView, ChangePasswordView, UserEditView, UserListView, RevisionListView, RoleAddView, RoleEditView, RoleListView, TemplateCollection) {
  'use strict';

  var Workspace = Backbone.Router.extend({
    initialize: function () {
      this.templateCollection = new TemplateCollection();
      this.layout = new BaseLayout({templateCollection: this.templateCollection});
    },
    routes: {
      '': 'frontpage',

      'categories/add': 'add_category',
      'categories/:id/edit': 'edit_category',
      'categories': 'category_list',

      'classificators/add': 'add_classificator',
      'classificators/:id/edit': 'edit_classificator',
      'classificators': 'classificator_list',

      'files/add': 'add_file',
      'files/add_existing': 'add_file_existing',
      'files/:id/edit': 'edit_file',
      'files': 'file_list',

      'import': 'import_view',

      'login': 'login_view',

      'logout': 'logout_view',

      'resources/:template_id/add': 'add_resource',
      'resources/:template_id/:id': 'edit_resource',
      'resources/:template_id/find/*query': 'resource_list',
      'resources/:template_id': 'resource_list',
      'geoview/:template_id/find/*query': 'resource_geoview',
      'geoview/:template_id': 'resource_geoview',

      'settings': 'settings_view',

      // order matters!
      'templates/layouts/add': 'add_template_layout',
      'templates/layouts/:id': 'edit_template_layout',
      'templates/layouts': 'template_layout_list',

      'templates/add': 'add_template',
      'templates/:id': 'edit_template',
      'templates/:id/clone': 'clone_template',
      'templates': 'template_list',
      // end of order matters!

      'users/add': 'add_user',
      'users/change_password': 'change_password',
      'users/:id': 'edit_user',
      'users': 'user_list',

      'revisions/:template_id/:resource_id': 'revision_list',

      'roles/add': 'add_role',
      'roles/:id': 'edit_role',
      'roles': 'role_list'
    },
    frontpage: function () {
      var view = new FrontPageView();
      this.layout.render(view);
    },

    add_category: function (params) {
      var view = new CategoryAddView();
      this.layout.render(view);
    },
    edit_category: function (categoryId) {
      var view = new CategoryEditView({categoryId: categoryId});
      this.layout.render(view);
    },
    category_list: function (params) {
      var view = new CategoryListView();
      this.layout.render(view);
    },

    add_classificator: function () {
      var view = new ClassificatorAddView();
      this.layout.render(view);
    },
    edit_classificator: function (classificatorId) {
      var view = new ClassificatorEditView({classificatorId: classificatorId});
      this.layout.render(view);
    },
    classificator_list: function () {
      var view = new ClassificatorListView();
      this.layout.render(view);
    },

    add_file: function (params) {
      var view = new FileAddView({templateCollection: this.templateCollection, params: params});
      this.layout.render(view);
    },
    add_file_existing: function (params) {
      var view = new FileAddExistingView({templateCollection: this.templateCollection, params: params});
      this.layout.render(view);
    },
    edit_file: function (fileId) {
      var view = new FileEditView({templateCollection: this.templateCollection, fileId: fileId});
      this.layout.render(view);
    },
    file_list: function (params) {
      var view = new FileListView({templateCollection: this.templateCollection, params: params});
      this.layout.render(view);
    },

    import_view: function (params) {
      var view = new ImportView({templateCollection: this.templateCollection, params: params});
      this.layout.render(view);
    },

    login_view: function (params) {
    	this.layout.initialize({templateCollection: this.templateCollection});
        var view = new LoginView({params: params, mainLayout: this.layout, tplCol: this.templateCollection});
      this.layout.render(view);
    },

    logout_view: function () {
      var view = new LogoutView();
      this.layout.render(view);
    },

    add_resource: function (templateId) {
      var view = new ResourceAddView({templateCollection: this.templateCollection, templateId: templateId});
      this.layout.render(view);
    },
    edit_resource: function (templateId, resourceId, params) {
      var view = new ResourceEditView({templateCollection: this.templateCollection, templateId: templateId, resourceId: resourceId, params: params});
      this.layout.render(view);
    },
    resource_list: function (templateId, query) {
      var view = new ResourceListView({templateCollection: this.templateCollection, templateId: templateId, query: query});
      this.layout.render(view);
    },
    resource_geoview: function (templateId, query) {
      var view = new ResourceGeoView({templateCollection: this.templateCollection, templateId: templateId, query: query});
      this.layout.render(view);
    },

    settings_view: function () {
      var view = new SettingsView();
      this.layout.render(view);
    },

    add_template: function () {
      var view = new TemplateAddView({templateCollection: this.templateCollection});
      this.layout.render(view);
    },
    edit_template: function (templateId) {
      var view = new TemplateEditView({templateCollection: this.templateCollection, templateId: templateId});
      this.layout.render(view);
    },
    clone_template: function (templateId) {
      var view = new TemplateAddView({templateCollection: this.templateCollection, templateId: templateId});
      this.layout.render(view);
    },
    template_list: function () {
      var view = new TemplateListView({collection: this.templateCollection});
      this.layout.render(view);
    },

    add_template_layout: function () {
      var view = new TemplateLayoutAddView();
      this.layout.render(view);
    },
    edit_template_layout: function (layoutId) {
      var view = new TemplateLayoutEditView({layoutId: layoutId});
      this.layout.render(view);
    },
    template_layout_list: function () {
      var view = new TemplateLayoutListView();
      this.layout.render(view);
    },

    add_user: function () {
      var view = new UserAddView();
      this.layout.render(view);
    },
    change_password: function () {
      var view = new ChangePasswordView();
      this.layout.render(view);
    },
    edit_user: function (userId) {
      var view = new UserEditView({userId: userId});
      this.layout.render(view);
    },
    user_list: function () {
      var view = new UserListView();
      this.layout.render(view);
    },

    revision_list: function (templateId, resourceId) {
      var view = new RevisionListView({templateId: templateId, resourceId: resourceId});
      this.layout.render(view);
    },

    add_role: function () {
      var view = new RoleAddView();
      this.layout.render(view);
    },
    edit_role: function (roleId) {
      var view = new RoleEditView({roleId: roleId});
      this.layout.render(view);
    },
    role_list: function () {
      var view = new RoleListView();
      this.layout.render(view);
    }
  });

  return Workspace;
});
