define(['backbone', 'models/revision'], function (Backbone, RevisionModel) {
  "use strict";

  var RevisionCollection = Backbone.Collection.extend({
    model: RevisionModel,

    initialize: function (models, options) {
      this.templateId = options.templateId;
      this.resourceId = options.resourceId;
    },

    url: function () {
      return '/api/revisions/' + this.templateId + '/' + this.resourceId;
    },

    parse: function (response) {
      return response.result;
    }
  });

  return RevisionCollection;
});
