define(['backbone', 'models/category'], function (Backbone, CategoryModel) {
  "use strict";

  var CategoryCollection = Backbone.Collection.extend({
    url: '/api/categories/',

    model: CategoryModel,

    parse: function (response) {
      return response.result;
    }
  });

  return CategoryCollection;
});
