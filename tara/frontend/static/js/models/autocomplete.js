define(['backbone'], function (Backbone) {
  "use strict";

  var AutocompleteModel = Backbone.Model.extend({
    initialize: function (attr, options) {
      this.options = options;
    },

    url: function () {
      return '/api/autocomplete/' + this.options.findBy + '/'+ this.options.field;
    }
  });

  return AutocompleteModel;
});
