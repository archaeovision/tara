define(['backbone'], function (Backbone) {
  "use strict";

  var CategoryModel = Backbone.Model.extend({
    idAttribute: '_id',

    urlRoot: '/api/categories/',

    initialize: function () {
      Backbone.Model.prototype.initialize.apply(this, arguments);
      this.on('change', function (model, options) {
        if (options && options.save === false) {
          return;
        }
        if (model.changed._id || model.changed._updated) {
          return;
        }
        model.save(model.changed, {patch: true});
      });
    },

    toString: function () {
      return this.get('name');
    }
  });

  return CategoryModel;
});
