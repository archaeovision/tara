define(['backbone'], function (Backbone) {
  "use strict";

  var UserModel = Backbone.Model.extend({
    idAttribute: '_id',

    urlRoot: '/api/users/',

    defaults: {
      password: ''
    },

    initialize: function () {
      Backbone.Model.prototype.initialize.apply(this, arguments);
      this.on('change', function (model, options) {
        if (options && options.save === false) {
          return;
        }
        if (model.changed._id) {
          return;
        }
        model.save(model.changed, {patch: true});
      });
    },

    toString: function () {
      return this.get('username');
    }
  });

  return UserModel;
});
