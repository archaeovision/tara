define(['backbone', 'models/autocomplete', 'backbone-forms', 'jquery-ui'], function (Backbone, AutocompleteModel) {
  "use strict";

  var InputAutocompleteEditor = Backbone.Form.editors.Text.extend({
    initialize: function (options) {
    	if( typeof options.schema.autocomplete.collection_name != 'undefined' && options.schema.autocomplete.collection_name.length ){
    		var findBy = options.schema.autocomplete.collection_name;
    	}
    	else {
    		var findBy = options.schema.autocomplete.template_id;
    	}
      Backbone.Form.editors.Text.prototype.initialize.call(this, options);
      var autocompleteModel = new AutocompleteModel({}, {findBy: findBy, templateId: this.schema.autocomplete.template_id, field: this.schema.autocomplete.field});
      if (!this.value) {
    	  this.value = this.schema.defaultValue;
      }
      this.$el.typeahead({source: function (query, process) {
        autocompleteModel.fetch({success: function () {
          process(autocompleteModel.toJSON().result);
        }});
      }});
    }
  });

  return InputAutocompleteEditor;
});