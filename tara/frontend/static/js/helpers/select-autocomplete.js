define(['backbone', 'models/autocomplete', 'backbone-forms'], function (Backbone, AutocompleteModel) {
  "use strict";

  var SelectAutocompleteEditor = Backbone.Form.editors.Select.extend({
    initialize: function (options) {
    	if( typeof options.schema.autocomplete.collection_name != 'undefined' && options.schema.autocomplete.collection_name.length ){
    		var findBy = options.schema.autocomplete.collection_name;
    	}
    	else {
    		var findBy = options.schema.autocomplete.template_id;
    	}
      var autocompleteModel = new AutocompleteModel({}, {findBy: findBy, field: options.schema.autocomplete.field});
      options.schema.options = function (result) {
        autocompleteModel.fetch({success: function () {
          var to_result = [
            {
              val: '',
              label: '--'
            }
          ];
          _.each(autocompleteModel.toJSON().result, function (resource) {
            to_result.push({
              val: resource,
              label: resource
            })
          });
          result(to_result);
        }});
      };
      delete options.schema.autocomplete;
      Backbone.Form.editors.Select.prototype.initialize.call(this, options);
    }
  });

  return SelectAutocompleteEditor;
});