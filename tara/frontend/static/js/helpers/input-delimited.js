define(['backbone', 'models/autocomplete', 'backbone-forms'], function (Backbone, AutocompleteModel) {
  "use strict";
/**
 * Added by Miina 08/10/2015
 * Object for managing text field dealing with array displayd as a string with separators
 */
  var InputDelimitedEditor = Backbone.Form.editors.Text.extend({
    initialize: function (options) {
    	this.$el.attr('type', 'text');
    	//If the separator is not set
        if( !options.schema.separator ) {
      	  this.separator = ',';
        }
        else {
      	  //Take the separator from options
        	this.separator = options.schema.separator;
        }
        //If autocomplete
        if( options.schema.autocomplete ){
        	if( typeof options.schema.autocomplete.collection_name != 'undefined' && options.schema.autocomplete.collection_name.length ){
        		var findBy = options.schema.autocomplete.collection_name;
        	}
        	else {
        		var findBy = options.schema.autocomplete.template_id;
        	}
          Backbone.Form.editors.Text.prototype.initialize.call(this, options);
          var autocompleteModel = new AutocompleteModel({}, {findBy: findBy, templateId: this.schema.autocomplete.template_id, field: this.schema.autocomplete.field});
          var self = this;
          this.$el.typeahead({source: function (query, process) {
            this.query = query;
        	  autocompleteModel.fetch({success: function () {
              process(autocompleteModel.toJSON().result);
            }});
          },
          updater: function(item) {
        	  var re = new RegExp( "[^" + self.separator + "]*$" );
              return this.query.replace( re, '' ) + item + self.separator;
          },
          matcher: function (item) {
            var tquery = self.extractLast(this.query);
            if(!tquery) return false;
            return ~item.toLowerCase().indexOf(tquery.toLowerCase())
          },
          highlighter: function (item) {
            
            var query = self.extractLast(this.query).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
            return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
              return '<strong>' + match + '</strong>'
            });
          }
        });
        }
        else {
        	Backbone.Form.editors.Text.prototype.initialize.call(this, options);
        }
    },
    
    extractLast : function( term ){
    	var re = new RegExp( '([^' + this.separator + ']+)$' );
    	var result = re.exec( term );
        if(result && result[1])
            return result[1].trim();
        return '';
    },
    
    setValue: function( value ) {
    	var self=this;
    	if( value ){
    		var split = value.replace(/(<([^>]+)>)/ig,"").split(this.separator);
    		var arr = '';
    		split.forEach(function (val, i) {
    			if( val.trim() ){
    				arr = arr + val + self.separator;
    			}
    		});
    		this.$el.val(arr);
    	}
    },
    
    getValue: function() {
    	var value = this.$el.val();
    	var arr = '';
    	if( value.trim() ){
    		var split = value.split(this.separator);
    		for (var i=0; i < split.length; i++){
    			if(split[i].trim()){
    				arr = arr + split[i] + this.separator;
    			}
    		}
    	}
    	return arr;
    }
    
  });
  return InputDelimitedEditor;
});
