define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";

  var TextAreaJSONEditor = Backbone.Form.editors.TextArea.extend({

    initialize: function(options) {
      // Call parent constructor
      if (!options.schema) {
        options.schema = {};
      }
      
      //If the gridType is not set
      if( !options.schema.gridFieldType ) {
    	  this.gridFieldType = '';
      }
      else {
    	  //Take the grid field type from options
    	  this.gridFieldType = options.schema.gridFieldType;
      }

      if (!options.schema.editorAttrs) {
        options.schema.editorAttrs = {};
      }

      if (!options.schema.editorAttrs.style) {
        options.schema.editorAttrs.style = 'font-family: monospace; width: 600px; height: 400px';
      } else {
        options.schema.editorAttrs.style += ' font-family: monospace;';
      }

      Backbone.Form.editors.Base.prototype.initialize.call(this, options);
    },

    getValue: function () {
      var value = this.$el.val();
      /*
       * Changed by Miina
       */
      if (value) {
    	  //If the element gridType is array
    	  if( this.gridFieldType == 'array' ){
    		  //Get the JSON value
    		  var jsonValue = JSON.parse(value);
    		  var returnValue = [];
    		  //Render the fields
    		  for(var i in jsonValue) {
    			  //Add value to the array returned
    			  returnValue[i] = jsonValue[i];
    		  }
    		  //Return the array
    		  return returnValue;
    	  }
    	  else {
    		  return JSON.parse(value);
    	  }
      }
    },
    setValue: function (value) {
      if (value) {
        var str = JSON.stringify(value, null, '  ');
        this.$el.val(str);
      }
    }
  });

  return TextAreaJSONEditor;
});
