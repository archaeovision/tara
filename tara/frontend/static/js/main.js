require.config({
  paths: {
    'async': 'libs/require/extensions/async',
    'backbone': 'libs/backbone/backbone',
    'backbone-forms': 'libs/backbone/extensions/backbone-forms',
    'backbone-pageable': 'libs/backbone/extensions/backbone-pageable',
    'backbone-queryparams': 'libs/backbone/extensions/backbone-queryparams',
    'backgrid': 'libs/backgrid/backgrid',
    'backgrid-paginator': 'libs/backgrid/extensions/backgrid-paginator',
    'bootstrap': 'libs/bootstrap',
    'filesaver': 'libs/filesaver/filesaver',
    'goog': 'libs/require/extensions/goog',
    'i18n': 'libs/require/extensions/i18n',
    'jquery': 'libs/jquery/jquery',
    'jquery-ui': 'libs/jquery-ui/jquery-ui.min',
    'lunr': 'libs/lunr/lunr',
    'openlayers': 'libs/openlayers/ol',
    'proj4': 'libs/proj4/proj4',
    'propertyParser': 'libs/require/extensions/propertyParser',
    'text': 'libs/require/extensions/text',
    'ucsv': 'libs/ucsv/ucsv',
    'underscore': 'libs/underscore/underscore',
    'underscore-string': 'libs/underscore/underscore-string',
    'underscore.template-helpers': 'libs/underscore/underscore.template-helpers'
  },
  shim: {
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'backbone-forms': {
      deps: ['backbone']
    },
    'backbone-pageable': {
      deps: ['backbone']
    },
    'backgrid': {
      deps: ['backbone'],
      exports: 'Backgrid'
    },
    'backgrid-paginator': {
      deps: ['backgrid']
    },
    'bootstrap/bootstrap-alert': {
      deps: ['jquery'],
      exports: '$.fn.alert'
    },
    'bootstrap/bootstrap-collapse': {
      deps: ['jquery'],
      exports: '$.fn.collapse'
    },
    'bootstrap/bootstrap-dropdown': {
      deps: ['jquery'],
      exports: '$.fn.dropdown'
    },
    'bootstrap/bootstrap-typeahead': {
      deps: ['jquery'],
      exports: '$.fn.typeahead'
    },
    'bootstrap/bootstrap-tooltip': {
        deps: ['jquery'],
        exports: '$.fn.tooltip'
    },
    'bootstrap/bootstrap-popover': {
        deps: ['jquery','bootstrap/bootstrap-tooltip'],
        exports: '$.fn.popover'
    },
    'filesaver': {
      exports: 'FileSaver'
    },
    'jquery': {
      exports: '$'
    },
    'jquery-ui': {
    	deps: ['jquery'],
        exports: '$.fn.draggable'
    },
    'openlayers': {
    	deps: ['proj4'],
    	exports: 'ol'
    },
    'proj4': {
        exports: 'proj4'
      },
    'ucsv': {
      exports: 'CSV'
    },
    'underscore': {
      exports: '_'
    },
    'underscore-string': {
      deps: ['underscore'],
      exports: '_s'
    },
    'underscore.template-helpers': {
      deps: ['underscore'],
      exports: '_s'
    }
  }
});

define(['routes', 'translator', 'underscore.template-helpers', 'underscore-string'], function (Workspace, T) {
  'use strict';
  
  var $body = $("body");

  $(document).on({
      ajaxStart: function() { $body.addClass("loading");
      },
       ajaxStop: function() { $body.removeClass("loading");
       }    
  });
  
  // We want also use translations in underscore templates.
  _.addTemplateHelpers({T: T});

  // Use absolute URLs to navigate to anything not in your Router.
  // Note: this version works with IE. Backbone.history.navigate will automatically route the IE user to the appropriate hash URL

  // Use delegation to avoid initial DOM selection and allow all matching elements to bubble
  $(document).delegate("a", "click", function (e) {
    // Get the anchor href and protcol
    var href = $(this).attr("href");
    var protocol = this.protocol + "//";

    // Ensure the protocol is not part of URL, meaning its relative.
    // Stop the event bubbling to ensure the link will not cause a page refresh.
    if (href && href.slice(protocol.length) !== protocol && protocol !== 'javascript://' && href.substring(0, 1) !== '#' && !href.match('^/api/.*') && this.target != '_blank'&& href != 'https://id.tara.ut.ee') {
      e.preventDefault();

      // Note by using Backbone.history.navigate, router events will not be
      // triggered.  If this is a problem, change this to navigate on your
      // router.
      Backbone.history.navigate(href, true);
    }
  });

  $(function () {
    Backbone.Message = {
      el: function () {
       return $(document).find('#error-container');
      },

      send: function (text, level) {
        this.clear();

        var tmpl = '<div class="alert <%= level %>"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <%= text %></div>';
        var html = _.template(tmpl, {'level': level, 'text': text});

        this.el().append(html);
      },

      send_error: function (text) {
        this.send(text, 'alert-error');
      },

      send_warning: function (text) {
        this.send(text, 'alert-block');
      },

      send_info: function (text) {
        this.send(text, 'alert-info');
      },

      send_success: function (text) {
        this.send(text, 'alert-success');
      },

      clear: function () {
        this.el().empty();
      }
    };

    Backbone.User = {
      _token: function (callback) {
        $.get('/api/auth/request_token', {}, function (res) {
          callback(res.token);
        });
      },

      _auth: function (username, password, token, callback) {
        $.ajax({
          url: '/api/auth/authenticate',
          type: 'post',
          headers: {
            'X-Tara-Token': token
          },
          data: {
            username: username,
            password: password
          },
          success: function (result) {
            if (result.error) {
              callback(result);
              return;
            }

            // setup ajax request for grant
            $.ajaxSetup({
              headers: {
                'X-Tara-Grant': result.grant
              }
            });

            // set authentication grant to localStorage
            localStorage.setItem('grant', result.grant);

            $.get('/api/users/'+result.user, function (res) {
              localStorage.setItem('user', JSON.stringify(res));
              $.get('/api/roles/'+res.role, function (res) {
                localStorage.setItem('roles', JSON.stringify(res));
              });
            });

            callback(result);
          }
        });
      },

      login: function (username, password, callback) {
        var self = this;
        self._token(function (token) {
          self._auth(username, password, token, callback);
        });
      },
      logout: function () {
        // call api to deauthorize user
        $.get('/api/auth/logout');

        // remove user information from browser localstorage
        localStorage.removeItem('grant');
        localStorage.removeItem('user');
        localStorage.removeItem('roles');

        // reset headers
        $.ajaxSetup({headers: null});
      },

      is_authenticated: function () {
        var user = JSON.parse(localStorage.getItem('user'));
        if (user) {
          return true;
        } else {
          return false;
        }
      },

      permissions: function (url) {
        if (!url) {
          url = '/' + Backbone.history.fragment;
        }

        var permissions = {};

        var roles = localStorage.getItem('roles');
        if (roles) {
          _.each(JSON.parse(roles).permissions, function (role) {
            if (url.match(role.url)) {
              _.each(role.methods, function (method) {
                permissions[method] = true
              });
            }
          });
        }

        return permissions;
      },

      get_grant: function () {
        return localStorage.getItem('grant');
      },

      get_info: function () {
        return JSON.parse(localStorage.getItem('user'));
      }
    };

    // global function to access permissions
    Backbone.View.permissions = function (url) {
      if (!url) {
        url = '/' + Backbone.history.fragment;
      }

      var permissions = {};

      var roles = localStorage.getItem('roles');
      if (roles) {
        _.each(JSON.parse(roles).permissions, function (role) {
          if (url.match(role.url)) {
            _.each(role.methods, function (method) {
              permissions[method] = true
            });
          }
        });
      }

      return permissions;
    };

    // set ajax requests for authentication
    // this used when user reloads browser
    var grant = localStorage.getItem('grant');
    if (grant) {
      $.ajaxSetup({
        headers: {
          'X-Tara-Grant': grant
        }
      });

      // rewrite user information on refresh
      $.ajax({
        url: '/api/users/' + JSON.parse(localStorage.getItem('user'))._id,
        statusCode: {
          401: function () {
            // remove user information from browser localstorage
            localStorage.removeItem('grant');
            localStorage.removeItem('user');
            localStorage.removeItem('roles');
          }
        },
        success: function (user) {
          localStorage.setItem('user', JSON.stringify(user));
          // set roles from fetched user
          $.get('/api/roles/'+user.role, function (roles) {
            localStorage.setItem('roles', JSON.stringify(roles));
          });
        }
      });
    }

    // setup code for 401 - not authenticated and 403 - not authorized.
    $.ajaxSetup({
      statusCode: {
        401: function () {
          if (Backbone.history.fragment != 'login') {
            localStorage.setItem('redirect', Backbone.history.fragment);
          }

          var cur = Backbone.history.fragment;
          if (cur.slice(0,15) == 'login?redirect=') {
            return;
          }

          var url = encodeURIComponent(cur);
          Backbone.history.navigate('login?redirect='+url, {trigger: true});
        },
        403: function () {
          ws.layout.$el.find('#content').html('<h1>Puuduvad õigused antud toimingu teostamiseks</h1>');
        }
      }
    });

    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      // Great success! All the File APIs are supported.
    } else {
      alert('The File APIs are not fully supported in this browser.');
    }

    // Run application
    var ws = new Workspace();
    Backbone.history.start({pushState: true});
  });
});