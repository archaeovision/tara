define(['backbone', 'helpers/textarea-json', 'helpers/textarea-json-small', 'helpers/select-null', 'translator'], function (Backbone, TextAreaJSONEditor, TextAreaJSONSmallEditor, SelectNullEditor, T) {
  "use strict";

  var TemplateForm = Backbone.Form.extend({
    initialize: function (options) {
      var self = this;

      var templateOptions = [{val: '', label: '--'}];
      _.each(options.templateCollection.toJSON(), function (template) {
        if(!template.parent && template._id != self.model.id) {
          templateOptions.push({
            val: template['_id'],
            label: template['label']
          });
        }
      });

      var templateLayoutOptions = [{val: '', label: '--'}];
      _.each(options.templateLayoutCollection.toJSON(), function (layout) {
        templateLayoutOptions.push({
          val: layout['_id'],
          label: layout['label']
        });
      });

      options.schema  = {
        label: {
          title: T('template-label'),
          type: 'Text',
          validators: ['required']
        },
		collection: {
          title: T('template-collection'),
          type: 'Text',
          //validators: ['required']
        },
/*		sublist_of: {
	          title: T('template-sublist-of'),
	          type: 'Text',
	          //validators: ['required']
	    },*/
        parent: {
          title: T('template-parent'),
          type: SelectNullEditor,
          options: templateOptions
        },
        layout: {
          title: T('template-layout'),
          type: SelectNullEditor,
          options: templateLayoutOptions
        },
        sort: {
          title: T('template-sort'),
          type: 'Number'
        },
        show_all_enabled: {
          title: T('template-show-all'),
          type: 'Checkbox'
        },
        unique: {
          title:T('template-unique'),
          type: TextAreaJSONSmallEditor
        },
        grid_fields: {
          title: T('template-grid-fields'),
          type: TextAreaJSONEditor,
          validators: ['required']
        },
        form_fields: {
          title: T('template-form-fields'),
          type: TextAreaJSONEditor,
          validators: ['required']
        }
      };

      Backbone.Form.prototype.initialize.call(this, options);
    }
  });

  return TemplateForm;
});
