define(['backbone', 'views/resource/widgets/file', 'views/resource/widgets/map', 'views/resource/widgets/detail', 'views/resource/widgets/reference', 'models/template_layout', 'collections/resource', 'text!/static/templates/resource/widgets/child.html'],
  function (Backbone, FileWidgetView, MapWidgetView, DetailWidget, ReferenceWidgetView, TemplateLayoutModel, ResourceCollection, childWidgetTmpl) {
    "use strict";

    var ChildWidgetView = Backbone.View.extend({
      initialize: function (options) {
        this.config = options.config;
        this.model = options.model;

        this.parentResourceCollection = new ResourceCollection(null, {templateId: this.config.template_id});

        this.templateCollection = options.templateCollection;
      },

      render: function () {
        var self = this;

        this.$el.append(childWidgetTmpl);

        // render files
        self.$el.append(self.renderFileWidget().el);

        var query = {};
        query[this.config.primary_key] = "^"+this.model[this.config.foreign_key]+"$";

        this.parentResourceCollection.fetch({
          data: query,
          success: function () {
            var result = self.parentResourceCollection.toJSON();
            if (result.length == 1) {
              self.resourceModel = result[0];

              // render map
              self.$el.append(self.renderMapWidget().el);

              // render parent info
              self.$el.append(self.renderDetailWidget().el);

              // render reference widget
              var layout_id = self.templateCollection.get(result[0]._template_id).toJSON().layout;

              var templateLayoutModel = new TemplateLayoutModel({_id: layout_id});
              templateLayoutModel.fetch({
                save: false,
                success: function (result) {
                  self.$el.append(self.renderReferenceWidget(result).el);
                }
              });
            }
          }
        });

        return this;
      },

      renderFileWidget: function () {
        var widget = new FileWidgetView({
          templateId: this.model._template_id,
          resourceId: this.model._id
        });

        return widget.render();
      },

      renderMapWidget: function () {
        var x_key = 'koordinaadid_X';
        var y_key = 'koordinaadid_Y';

        if (this.config && this.config.coordinates_x_key) {
          x_key = this.config.coordinates_x_key;
          y_key = this.config.coordinates_y_key;
        }

        var widget = new MapWidgetView({height: 500, coord_x: this.resourceModel[x_key], coord_y: this.resourceModel[y_key]});

        return widget.render();
      },

      renderDetailWidget: function () {
        var template = this.templateCollection.get(this.config.template_id).toJSON();

        var widget = new DetailWidget({
          template: template,
          model: this.resourceModel
        })

        return widget.render();
      },

      renderReferenceWidget: function (widget_config) {
        var config = widget_config.toJSON().widgets[0].config;
        var widget = new ReferenceWidgetView({config: config, templateCollection: this.templateCollection, model: this.resourceModel});
        return widget.render();
      }
    });

    return ChildWidgetView;
  });
