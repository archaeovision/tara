define(['backbone', 'views/resource/widgets/file', 'views/resource/widgets/map', 'views/resource/widgets/reference', 'text!/static/templates/resource/widgets/parent.html'],
function (Backbone, FileWidgetView, MapWidgetView, ReferenceWidgetView, parentWidgetTmpl) {
  "use strict";

  var ParentWidgetView = Backbone.View.extend({
    initialize: function (options) {
      this.config = options.config;
      this.model = options.model;
      this.templateCollection = options.templateCollection;
    },

    render: function () {
      this.$el.append(parentWidgetTmpl);

      // render files
      this.$el.append(this.renderFileWidget().el);

      // render referencs
      this.$el.append(this.renderReferenceWidget().el);

      // render map
      this.$el.append(this.renderMapWidget().el);

      return this;
    },

    renderFileWidget: function () {
      var widget = new FileWidgetView({
        templateId: this.model._template_id,
        resourceId: this.model._id
      });

      return widget.render();
    },


    renderMapWidget: function () {
      var x_key = 'latitude';
      var y_key = 'longitude';
      var proj="";

      if (this.config && this.config.coordinates_x_key) {
        x_key = this.config.coordinates_x_key;
        y_key = this.config.coordinates_y_key;
      }

	  if (this.config && this.config.projection) {
        proj = this.config.projection;
      }

      var widget = new MapWidgetView({height: 500, coord_x: this.model[x_key], coord_y: this.model[y_key],projection:proj});

      return widget.render();
    },

    renderReferenceWidget: function () {
      var widget = new ReferenceWidgetView({config: this.config, templateCollection: this.templateCollection, model: this.model});
      return widget.render();
    }
  });

  return ParentWidgetView;
});
