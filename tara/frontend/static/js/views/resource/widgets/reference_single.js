define(['backbone', 'collections/resource', 'text!/static/templates/resource/widgets/reference_single.html'],
function (Backbone, ResourceCollection, referenceWidgetTmpl) {
  "use strict";

  var ReferenceWidgetView = Backbone.View.extend({
    events: {
      'click .panel-title': function (e) {
        var item = $(e.target).attr('href');
        var key = '_widget_reference2';

        var res = localStorage.getItem(key);
        if (res) {
          var parsed = JSON.parse(res);
          var position = parsed.indexOf(item);

          if (position == -1) {
            parsed.push(item);
          } else {
            parsed.splice(position, 1);
          }

          localStorage.setItem(key, JSON.stringify(parsed))
        } else {
          localStorage.setItem(key, JSON.stringify([item]))
        }
      }
    },

    initialize: function (options) {
      var self = this;
      this.config = options.config;
      this.model = options.model;
      this.templateCollection = options.templateCollection;
      
      this.reference=options.reference;
      this.collections = [];

      _.each(this.config.references.list, function (ref) {
        var url = '/resources/' + ref.template_id;
        if (Backbone.View.permissions(url).GET) {
          var query = {};
          query[ref.foreign_key] = '^' + self.model[self.config.references.primary_key] + '$';

          var resourceCollection = new ResourceCollection([], {
            templateId: ref.template_id,
            state: {
              pageSize:1 
            },
            queryParams: query
          });

          self.listenTo(resourceCollection, 'reset', self.renderReference);
          self.collections.push(resourceCollection);
        }
      });
    },

    render: function () {
      _.each(this.collections, function (collection) {
        collection.fetch({reset: true});
      });
      return this;
    },

    renderReference: function (result) {
      var template = this.templateCollection.get(result.templateId).toJSON();

      // map labels from template
      var labels = {};
      _.each(template.grid_fields, function (field) {
        labels[field.name] = field.label;
      });

      // find reference config by template_id
      var reference_config = _.find(this.config.references.list, function (ref) { return ref.template_id == result.templateId; });

      // 'translate' only specifyed keys from
      var fields = [];
      _.each(reference_config.grid_fields, function (field) {
        fields.push({key: field, label: labels[field]});
      });

      var url_filter = reference_config.foreign_key + '=^' + this.model[this.config.references.primary_key] + '$';
      var widget_name = this.config.references.wname || template.label;
      var widget_id = widget_name.replace(/ /g, '-') + '-' + this.reference;
      var tmpl = _.template(referenceWidgetTmpl, {wname: widget_name, wid: widget_id, template: template, fields: fields, resources: result, url_filter: url_filter});
      this.$el.append(tmpl);

      var self = this;

      var res = localStorage.getItem('_widget_reference2');
      if (res) {
    	  try {
	        _.each(JSON.parse(res),function (item) {
	          self.$el.find(item).collapse('show');
	        });
    	  }
    	  catch(e){}
      }
    }
  });

  return ReferenceWidgetView;
});
