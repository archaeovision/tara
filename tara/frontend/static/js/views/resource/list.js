define(['collections/resource', 'models/autocomplete', 'views/resource/grid', 'ucsv', 'text!/static/templates/resource/list.html', 'translator', 'backgrid-paginator', 'filesaver'],
function (ResourceCollection, AutocompleteModel, ResourceGrid, CSV, resourceListTmpl, T) {
  "use strict";

  var ResourceListView = Backbone.View.extend({
    template: _.template(resourceListTmpl),

    events: {
      'click #filter-template': function (e) {
        if ($(e.target).is(':checked') == true) {
          this.resourceCollection.templateId = this.templateCollection.get(this.templateId).get('parent');
        } else {
          this.resourceCollection.templateId = this.templateId;
        }
        this.resourceCollection.fetch();
      },

      'change #page-size': function (e) {
        if(e.target.value) {
          var val = parseInt(e.target.value);
          this.resourceCollection.setPageSize(val);
        }
      },

      /* Redirect client when choosing subtemplate */
      'change #redirect-child': function (e) {
        if(e.target.value) {
          Backbone.history.navigate('resources/' + e.target.value, {trigger: true});
        }
      },

      /* Update autocomplete searching */
      'change #field-filter': function (e) {
        var self = this;
        var selected = $(e.target).find('option:selected');

        if (e.target.value && selected.data('type') != 'number') {
          var input = this.$el.find('input');
          input.val('');
          input.typeahead({
            source: function (query, process) {
              var autocomplete = new AutocompleteModel({}, {templateId: self.templateId, field: e.target.value});
              autocomplete.fetch({success: function () {
                process(autocomplete.toJSON().result);
              }});
            },
            updater: function (item) {
              return '^' + item + '$';
            }
          });
        }
      },

      /* Filter search form submit */
      'submit form': function (e) {
        e.preventDefault();

        var key = e.target[0].value;
        var val = e.target[1].value;

        if (key && val) {
          var qp = this.resourceCollection.queryParams;
          if (qp[key]) {
            if (!_.isArray(qp[key])) {
              qp[key] = [qp[key]];
            }
            if (_.indexOf(this.resourceCollection.queryParams[key], val) == -1) {
              qp[key].push(val);
            }
          } else {
            qp[key] = val;
          }
        } else {
          return;
        }

        // reset input value
        $(e.target[1]).val('');

        this.resourceCollection.fetch();

        // go to first page
        this.resourceCollection.state.currentPage = 1;
      },

      /* Remove filter */
      'click span[class="label"]': function (e) {
        var result = e.target.textContent.split('=');
        var key = result[0];
        var val = result[1];

        // Removes item from array (if it is array)
        var qp = this.resourceCollection.queryParams;

        if (qp[key]) {
          if (_.isArray(qp[key])) {
            var pos = _.indexOf(qp[key], val);
            if (pos != -1) {
              qp[key].splice(pos, 1);
            }
          } else {
            delete qp[key];
          }
        }

        e.target.remove();
        this.resourceCollection.fetch();

        // go to first page
        this.resourceCollection.state.currentPage = 1;
      },

      /* CSV export */
      'click #export-resources': function (e) {
        // 'Kannatust, faili allalaadimine algab hetke pärast'
        Backbone.Message.send_info(T('please-wait-starting-download'));

        // disable button
        $(e.target).attr('disabled', 'disabled');

        var fields = this.templateCollection.get(this.templateId).get('form_fields');

        // Labels for first row
        var row = [];
        for (var i = 0; i < fields.length; i++) {
          if (fields[i].type != 'metadata') {
            row.push(fields[i].label);
          }
        }

        var result = [row];

        var process = function () {
          _.each(resources.models, function (model) {
            var row = [];
            _.each(fields, function (field) {
              if (field.type != 'metadata') {
                if (field.type == 'classificator_multi') {
                  row.push('['+model.get(field.name)+']');
                } else {
                  row.push(model.get(field.name) || '');
                }
              }
            });
            result.push(row);
          });

          if (resources.hasNext()) {
            resources.getNextPage();
          } else {
            $(e.target).removeAttr('disabled');
            var str = CSV.arrayToCsv(result);
            saveAs(new Blob([str], {type: 'text/csv'}), 'export.csv');
            // 'Faili genereerimine õnnestus, alustan allalaadimist'
            Backbone.Message.send_success('csv-export-success');
          }
        };

        var resources = new ResourceCollection([], {templateId: this.templateId});
        resources.queryParams = this.resourceCollection.queryParams;

        resources.setPageSize(1500);

        this.listenTo(resources, 'sync', process);
      }
    },

    initialize: function (options) {
      this.templateId = options.templateId;

      this.templateCollection = options.templateCollection;

      this.resourceCollection = new ResourceCollection([], {templateId: options.templateId});
      this.reserved_qp = _.clone(this.resourceCollection.queryParams);
      // set sorting
      if (options.query && _.has(options.query, 'sort_by') && _.has(options.query, 'order')) {
        this._sort_by = options.query['sort_by'];
        this._order = options.query['order'];
        delete options.query['sort_by'];
        delete options.query['order'];
      }

      // set page
      if (options.query && _.has(options.query, 'page')) {
        this.resourceCollection.state['currentPage'] = parseInt(options.query['page']);
        delete options.query['page'];
      }

      // set page size
      if (options.query && _.has(options.query, 'per_page')) {
        this.resourceCollection.setPageSize(parseInt(options.query['per_page']));
        delete options.query['per_page'];
      }

      // query show_all
      if (options.query && _.has(options.query, 'show_all')) {
        this.show_all = true;
        delete options.query['show_all'];
      }

      // set query params
      var self = this;
      _.each(options.query, function (val, key) {
        self.resourceCollection.queryParams[key] = val;
      });
      this.justinitialized=true;
      // event used for hidding or displaying paginator, depending on page count
      this.listenTo(this.resourceCollection, 'sync', this.after_sync);
    },

    display_badge: function (key, val) {
      var el = $('<span><span class="label">' + key + '=' + val +'</span>&nbsp;&nbsp;</span>');
      this.$el.find('#filter-list').append(el);
    },

    after_sync: function (collection) {

      var self = this;
      this.$el.find('#filter-list').empty();

      var url = Backbone.history.fragment;
      // clear url query params
      if (_.contains(url, '?')) {
        url = url.split('?')[0];
      }

      var urlvars=this.loadQueryParams();

      // starting of query params
      url += '?';

      _.each(self.resourceCollection.queryParams, function (val, key) {
        if(!_.has(self.reserved_qp, key)) {
          if (_.isArray(val)) {
            _.each(val, function (val) {
              self.display_badge(key, val);
              url += key + '=' + val + '&';
            });
          } else {
            self.display_badge(key, val)
            url += key + '=' + val + '&';
          }
        }
      });

      // add sort by query param
      if (this.resourceCollection.state['sortKey']) {
        url += 'sort_by' + '=' + this.resourceCollection.state['sortKey'] + '&';

        var order = this.resourceCollection.state['order'] == -1 ? 'ascending' : 'descending';
        url += 'order' + '=' + order + '&';
      }

      if (this.justinitialized && urlvars['page'] && parseInt(urlvars['page'])>0) {
        //url += 'page' + '=' + urlvars['page'] + '&';
        this.resourceCollection.state['currentPage'] = parseInt(urlvars['page']);
        this.justinitialized=false;
      }

      // add current page
      if (this.resourceCollection.state['currentPage'] /*&& this.resourceCollection.state['currentPage'] != 1*/) {
        url += 'page' + '=' + this.resourceCollection.state['currentPage'] + '&';
        url += 'per_page' + '=' + this.resourceCollection.state['pageSize'] + '&';
      }



      if (this.resourceCollection.templateId != this.templateId) {
        url += 'show_all' + '=true&';
        this.$el.find('#filter-template').prop('checked', true);
      }

      // remove last &
      url = url.substring(0, url.length - 1);

      /* set new url if currnet url ne composed url*/
      if (decodeURIComponent(Backbone.history.fragment) != url) {
        Backbone.history.navigate(url);
      }

      // show or hide paginator depending on page count
      if(collection.state.totalPages < 2) {
        this.$el.find('#grid-content').find('.backgrid-paginator').hide();
      } else {
        this.$el.find('#grid-content').find('.backgrid-paginator').show();
      }

      // show user total count of items in collection
      this.$el.find('#total-count').show();
      this.$el.find('#total-count-nr').html(collection.state.totalRecords);
    },

      /**
       * @todo Kaarel added, maybe needed to add to other places also
      */
    loadQueryParams: function(vars) {
      if (!vars) vars=[];
      var url = Backbone.history.fragment;
      var request = {};
      var pairs = url.substring(url.indexOf('?') + 1).split('&');
      for (var i = 0; i < pairs.length; i++) {
        if(!pairs[i])
            continue;
        var pair = pairs[i].split('=');
        vars[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
      }
      return vars;

    },
    render: function () {
    	self=this;
      var html = this.template({templates: this.templateCollection, templateId: this.templateId, permissions: Backbone.View.permissions()});
      this.$el.append(html);

      // render grid
      var templateModel = this.templateCollection.get(this.templateId);
      this.grid = new ResourceGrid({collection: this.resourceCollection, grid_fields: templateModel.attributes.grid_fields});
      this.$el.find('#grid-content').append(this.grid.render().$el);

      /**
         * If no sorting from UI, get one from configuration
      */
      if (!this._sort_by || !this._order) {
          _.each(this.templateCollection.get(this.templateId).get('grid_fields'),function(i) {
            if (i['default_sort']) {
              self._sort_by=i['name'];
              if (i['default_order']=='ascending' || i['default_order']=='asc') {
                self._order='ascending';
              }
              else self._order='descending';
            }
          });
      }


      // sort grid
      if (this._sort_by && this._order) {
        var self = this;
        _.each(this.grid.header.row.cells, function (item) {
          if (item.column.attributes.name == self._sort_by) {
            item.sort(self._sort_by, self._order);
          }
        });
      }

      // render paginator
      this.paginator = new Backgrid.Extension.Paginator({collection: this.resourceCollection, goBackFirstOnSort: true});
      this.$el.find('#grid-content').append(this.paginator.render().$el);

      if (this.show_all) {
        this.resourceCollection.templateId = this.templateCollection.get(this.templateId).get('parent');
      }
      this.resourceCollection.fetch();

      return this;
    },

    onClose: function () {
      if (this.filter) {
        this.filter.remove();
      }
      if (this.paginator) {
        this.paginator.remove();
      }
      if (this.grid) {
        this.grid.remove();
      }
    }
  });

  return ResourceListView;
});
