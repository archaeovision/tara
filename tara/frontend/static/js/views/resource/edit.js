define([
  'backbone',
  'models/template',
  'models/template_layout',
  'models/resource',
  'views/resource/form',
  'text!/static/templates/resource/edit.html',
  'text!/static/templates/resource/form.html',
  'translator'
], function (Backbone, TemplateModel, TemplateLayoutModel, ResourceModel, ResourceForm, resourceEditTmpl, resourceFormTmpl, T) {
  'use strict';

  var ResourceEditView = Backbone.View.extend({
    events: {
      'click #edit-form': function (e) {
        e.preventDefault();
        this.$el.find('#resource-form').toggle();
        this.$el.find('#resource-table').toggle();
        this.$el.find('#form-actions').toggle();

        var x = this.$el.find('#change-view-text');
        if (x.text() == T('resource-button-start-editing')) {
          x.text(T('resource-button-stop-editing'));
        } else {
          x.text(T('resource-button-start-editing'));
        }
      },
      'click #save-item': function (e) {
        e.preventDefault();
        var errors = this.form.commit();
        if (errors) {
          Backbone.Message.send_warning(T('resource-error-save'));
        }
      },
      'click #delete-item': function (e) {
        var self = this;
        e.preventDefault();
        if(confirm(T('confirm-delete'))) {
          this.model.destroy({success: function () {
            Backbone.Message.send_info(T('resource-deleted'));
            Backbone.history.navigate('resources/' + self.templateId, {trigger: true});
          }});
        }
      }
    },

    widgets: [],

    initialize: function (options) {
      this.options = options;
      this.templateCollection = options.templateCollection;
      this.templateId = options.templateId;
      this.resourceId = options.resourceId;

      this.model = new ResourceModel({_id: options.resourceId}, {templateId: this.templateId});
      this.listenTo(this.model, 'sync', this.redirect);
    },

    render: function () {
      var self = this;
      var parentTempmateModel;
      this.templateModel = this.templateCollection.get(this.templateId).toJSON();
      if(this.templateModel.parent) {
        parentTempmateModel = this.templateCollection.get(this.templateModel.parent);
      }

      // fetch model and render form
      this.model.fetch({save: false, success: function () {
    	  /*
    	   *Added by Miina. 
    	   */
    	  if(typeof self.model.get('_template_id') == 'undefined') {
    		  var revisions_url = '/revisions/' + self.model.templateId + '/' + self.model.id;
    	  }
    	  else {
    		  var revisions_url = '/revisions/' + self.model.get('_template_id') + '/' + self.model.id;
    	  }

        var tmpl = _.template(resourceEditTmpl, {
          label: self.templateModel.label,
          form_fields: self.templateModel.form_fields,
          model: self.model,
          parentTemplate: parentTempmateModel,
          revisions_url: revisions_url
        });
        self.$el.append(tmpl);
        // show revisions
        if (Backbone.User.permissions('/revisions/'+self.templateId).GET) {
          $.get('/api'+revisions_url, function(data) {
            if (data.result.length > 0) {
              self.$el.find('#revision_history').show();
            }
          });
        }

        self.form = new ResourceForm({template: _.template(resourceFormTmpl), form_fields: self.templateModel.form_fields, model: self.model});
        self.$el.find('#resource-form').append(self.form.render().el);

        if (self.options.params && self.options.params.edit == 'true') {
          self.$el.find('#edit-form').trigger('click');
        }
        self.renderWidgets();
      },
		error:function(collection, response, options) {
		}
	
	});

      return this;
    },

    redirect: function (model, resp, options) {
      if (options.patch) {
        Backbone.history.history.back();
      }
    },

    renderWidgets: function () {
        var self = this;

        var model = self.model.toJSON();

        // get layout and render widgets
        if (this.templateModel.layout) {
          var templateLayoutModel = new TemplateLayoutModel({_id: this.templateModel.layout});
          templateLayoutModel.fetch({
            save: false,
            success: function () {
          	  var i = 0;
          	  var k = 0;
              _.each(templateLayoutModel.toJSON().widgets, function (widget) {
              	i++;
                requirejs([widget.path], function (Widget) {
              	  k++;
                  var widgetInstance = new Widget({
                    config: widget.config,
                    model: model,
                    reference: k,
                    templateCollection:self.templateCollection
                  });
                  self.$el.find('#resource-widgets').append(widgetInstance.render().el);
                  self.widgets.push(widgetInstance);
                });
              });
            }
          });
        }
      },

    onClose: function () {
      _.each(this.widgets, function (widget) {
        if (widget.onClose) {
          widget.onClose();
        }
        widget.remove();
      });

      if (this.form) {
        this.form.remove();
      }
    }
  });

  return ResourceEditView;
});
