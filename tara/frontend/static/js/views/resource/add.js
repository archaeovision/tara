define(['backbone', 'models/template', 'models/resource', 'views/resource/form', 'text!/static/templates/resource/add.html'], function (Backbone, TemplateModel, ResourceModel, ResourceForm, resourceAddTmpl) {
  "use strict";

  var ResourceAddView = Backbone.View.extend({
    template: _.template(resourceAddTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      }
    },

    initialize: function (options) {
      this.templateId = options.templateId;
      this.templateCollection = options.templateCollection;
    },

    render: function () {
      var model = new ResourceModel({}, {'templateId': this.templateId});
      var templateModel = this.templateCollection.get(this.templateId);

      this.form = new ResourceForm({template: this.template, form_fields: templateModel.attributes.form_fields, model: model});
      this.$el.append(this.form.render().el);

      this.listenTo(model, 'sync', this.redirect);

      return this;
    },

    redirect: function (model) {
      Backbone.history.navigate('resources/'+this.templateId , {trigger: true});
      Backbone.history.navigate('resources/'+this.templateId + '/' + model.id, {trigger: true});
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return ResourceAddView;
});
