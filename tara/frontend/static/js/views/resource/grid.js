define(['backgrid', 'translator'], function (Backgrid, T) {
  "use strict";

  var ResourceGrid = Backgrid.Grid.extend({
    initialize: function (options) {
      options.columns = [];

      options.columns.push({
        name: '_',
        label: '',
        editable: false,
        cell: Backgrid.Cell.extend({
          render: function () {
            if(this.model.id) {
              //var template = ' <a href="/resources/<%= _template_id %>/<%= _id %>"> <i class="icon-eye-open"></i> </a>';
              var template = ' <a href="/resources/'+this.model.collection.templateId+'/<%= _id %>"> <i class="icon-eye-open"></i> </a>';
              this.$el.append(_.template(template, this.model.attributes));
            }
            return this;
          }
        })
      });

      if (Backbone.User.is_authenticated() && Backbone.User.permissions().PATCH) {
        options.columns.push({
          name: '_action',
          label: 'Muuda',
          editable: false,
          cell: Backgrid.Cell.extend({
            render: function () {
              if(this.model.id) {
                  //var template = '<a href="/resources/<%= _template_id %>/<%= _id %>?edit=true" class="btn edit">' + T('button-edit') + '</a>';
                    var template = '<a href="/resources/'+this.model.collection.templateId+'/<%= _id %>?edit=true" class="btn edit">' + T('button-edit') + '</a>';

                  this.$el.append(_.template(template, this.model.attributes));
              }
              return this;
            }
          })
        });
      }

      _.each(options.grid_fields, function (item) {
		/** @todo unnice exception
		**/
        if (item.type=="uri") {
			options.columns.push({
          name: item.name,
          label: item.label,
          cell: Backgrid.UriCell.extend({
			  render: function(){
				    this.$el.html(this.model.get(this.column.get("name")));
				    return this;
				}
			}),
          editable: false
        });
		}
        else if ( item.type =='delimited' ){
        	options.columns.push({
	        	name: item.name,
	        	label: item.label,
	        	cell: Backgrid.UriCell.extend({
	        		render: function() {
	    				    this.$el.html(this.model.get(this.column.get("name")));
	    				    return this;
	        		}	
	        	}),
	        	editable: false
        	});
        }
        else if( item.type=='date') {
			options.columns.push({
				name: item.name,
				label: item.label,
				cell: 'date',
				editable: false
			});
		}
        else {   
        options.columns.push({
          name: item.name,
          label: item.label,
          cell: 'string',
          editable: false
        });
		}
      });

      delete options.grid_fields;

      this.constructor.__super__.initialize.apply(this, [options]);
    }
  });

  return ResourceGrid;
});
