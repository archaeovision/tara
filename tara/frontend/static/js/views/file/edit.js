define(['backbone', 'views/file/form', 'models/file', 'text!/static/templates/file/edit.html', 'translator', 'filesaver'],
function (Backbone, FileForm, FileModel, fileEditTmpl, T) {
  "use strict";

  var FileEditView = Backbone.View.extend({
    template: _.template(fileEditTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        var errors = this.form.commit();
        if(!errors) {
          Backbone.history.navigate('files', {trigger: true});
        }
      },
      'click #delete-item': function (e) {
        e.preventDefault();
        if(confirm(T('confirm-delete'))) {
          this.model.destroy({success: function () {
            //Backbone.history.navigate('files', {trigger: true});
            Backbone.history.history.back();
          }});
        }
      },
      'click #download-file': function (e) {
        var target = $(e.target);
        target.attr('disabled', 'disabled');

        var file_id = target.data('id');

        $.get('/api/files/' + file_id, function (file_info) {
          var url = '/api/download/' + file_id;

          if (!Backbone.User.is_authenticated() && file_info.contentType == 'image/jpeg') {
            url += '?w=800&h=600';
          }

          var xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function () {
              if (this.readyState == 4 && this.status == 200) {
                target.removeAttr('disabled');
                saveAs(this.response, file_info.filename);
              }
          }

          xhr.open('GET', url);

          if (Backbone.User.is_authenticated()) {
            xhr.setRequestHeader('X-Tara-Grant', Backbone.User.get_grant());
          }

          xhr.responseType = 'blob';
          xhr.send();
        });
      }
    },

    initialize: function (options) {
      this.model = new FileModel({_id: options.fileId});
      this.templateCollection = options.templateCollection;
    },

    render: function () {
      var self = this;

      this.model.fetch({
        save: false,
        success: function () {
          self.$el.append(self.template({
            fileId: self.model.id,
            file: self.model.attributes,
            permissions: Backbone.View.permissions()
          }));

          var tmpl = _.template('<div data-fields="*"></div>');
          self.form = new FileForm({template: tmpl, templateCollection: self.templateCollection, model: self.model});
          self.$el.find('#form-content').append(self.form.render().el);
        }
      });
      return this;
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return FileEditView;
});
