define(['backbone', 'text!/static/templates/frontpage/index.html'], function (Backbone, frontPageTmpl) {
  "use strict";

  var FrontPageView = Backbone.View.extend({
    template: _.template(frontPageTmpl),

    render: function () {
      this.$el.append(this.template);
      return this;
    }
  });

  return FrontPageView;
});
