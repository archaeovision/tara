define(['backgrid', 'translator'], function (Backgrid, T) {
  "use strict";

  var CategoryGrid = Backgrid.Grid.extend({
    initialize: function (options) {
      options.columns = [
        {
          name: '_action',
          label: '',
          editable: false,
          cell: Backgrid.Cell.extend({
            render: function () {
              if(this.model.id) {
                var template = '<a href="/categories/<%= _id %>/edit" class="btn edit">' + T('button-edit') + '</a>';
                this.$el.append(_.template(template, this.model.attributes));
              }
              return this;
            }
          })
        },
        {
          name: 'name',
          label: T('category-name'),
          editable: false,
          cell: 'string'
        },
        {
          name: 'sort',
          label: T('category-sort'),
          editable: false,
          cell: 'string'
        },
        {
          name: '_inserted',
          label: T('meta-added'),
          editable: false,
          cell: 'string',
          formatter: {
            fromRaw: function (value) {
              return new Date(value).toLocaleString();
            }
          }
        }
      ];
      this.constructor.__super__.initialize.apply(this, [options]);
    }
  });

  return CategoryGrid;
});
