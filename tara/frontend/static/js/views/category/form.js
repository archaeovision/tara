define(['translator', 'helpers/select-autocomplete', 'backbone-forms'], function (T, SelectAutocompleteEditor) {
  "use strict";

  var CategoryForm = Backbone.Form.extend({
    schema: {
      name: {
        type: 'Text',
        title: T('category-name'),
        validators: ['required']
      },
      url: {
        type: 'Text',
        title: T('category-url')
      },
      parent: {
    	 type: SelectAutocompleteEditor,
    	 autocomplete: {collection_name:'categories',field:'name'}
      },
      icon: {
        type: 'Text',
        title: T('category-icon')
      },
      is_external: {
        type: 'Checkbox',
        title: T('category-is-external')
      },
      sort: {
        type: 'Number',
        title: T('category-sort')
      }
    },

    initialize: function (options) {
      Backbone.Form.prototype.initialize.call(this, options);
    }
  });

  return CategoryForm;
});
