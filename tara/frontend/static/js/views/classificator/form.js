define(['translator', 'backbone-forms'], function (T) {
  "use strict";

  var ClassificatorForm = Backbone.Form.extend({
    schema: {
      name: {
        type: 'Text',
        title: T('classificator-name'),
        validators: ['required']
      },
      value: {
        type: 'Text',
        title: T('classificator-value'),
        validators: ['required']
      },
      type: {
        type: 'Text',
        title: T('classificator-type'),
        validators: ['required']
      },
      sort: {
        type: 'Number',
        title: T('classificator-sort')
      },
      description: {
        type: 'TextArea',
        title: T('classificator-description')
      }
    },

    initialize: function (options) {
      Backbone.Form.prototype.initialize.call(this, options);
    }
  });

  return ClassificatorForm;
});
