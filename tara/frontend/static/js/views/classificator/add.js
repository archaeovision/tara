define(['backbone', 'views/classificator/form', 'models/classificator', 'text!/static/templates/classificator/add.html'],
function (Backbone, ClassificatorForm, ClassificatorModel, classificatorAddTmpl) {

  "use strict";

  var ClassificatorAddView = Backbone.View.extend({
    template: _.template(classificatorAddTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        var errors = this.form.commit();
        if (!errors) {
          Backbone.history.navigate('classificators', {trigger: true});
        }
      }
    },

    initialize: function () {
      var model = new ClassificatorModel();
      this.form = new ClassificatorForm({template: this.template, model: model});
    },

    render: function () {
      this.$el.append(this.form.render().el);
      return this;
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return ClassificatorAddView;
});
