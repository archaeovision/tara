define(['collections/revision', 'text!/static/templates/revision/list.html'], function (RevisionCollection, revisionListTmpl) {
  "use strict";

  var RevisionListView = Backbone.View.extend({
    template: _.template(revisionListTmpl),

    initialize: function (options) {
      this.collection = new RevisionCollection([], options);
      this.listenTo(this.collection, 'reset', this.update);

    },

    render: function () {
      this.collection.fetch({reset: true});
      return this;
    },

    update: function () {
      var html = this.template({revisions: this.collection});
      this.$el.append(html);
    }
  });

  return RevisionListView;
});
