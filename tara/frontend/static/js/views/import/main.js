define(['backbone', 'ucsv', 'helpers/input-file', 'models/resource', 'text!/static/templates/import/index.html', 'text!/static/templates/import/form.html', 'translator'],
function (Backbone, CSV, InputFileEditor, ResourceModel, importTmpl, importFormTmpl, T) {
  'use strict';

  var ImportView = Backbone.View.extend({
    template: _.template(importTmpl),

    events: {
      'click #preview-item': function (e) {
        var self = this;
        e.preventDefault();

        // table preview jquery element
        var preview_table = this.$el.find('#import-preview');

        // clear preview
        preview_table.find('thead > tr').empty();
        preview_table.find('tbody').empty();

        // validate form
        var errors = this.form.validate();

        if(errors) {
          // 'Fail või kogu jäi valimata.'
          Backbone.Message.send_error(T('import-error-file-or-collection-missing'));

          this.$el.find('#start-import').hide();

        } else {
          this.$el.find('#start-import').show();

          // fetch template form_fields
          var template_form_fields = this.templateCollection.get(this.form.getValue('template')).toJSON().form_fields;

          // fill table header
          _.each(template_form_fields, function (field) {
            preview_table.find('thead > tr:last').append('<th>' + field.label + '</th>');
          });

          // read local file using HTML5 FileReader API
          var fileRead = new FileReader();
          fileRead.onload = function () {
            // Store file content in view object to use it later on saving
            self.fileReaderResult = fileRead.result;

            // Parse file content
            var result = CSV.csvToArray(fileRead.result);

            // Skip header/first row?
            if (self.$el.find('#skip-first-row').is(':checked')) {
              result.shift();
            }

            // Display preview by appending rows to table
            for (var i = 0; i < 10; i++) {
              var row = result.shift();

              // Break from loop when we dont have any rows to display anymore
              if(!row) {
                break;
              }

              // create new row
              preview_table.find('tbody').append('<tr></tr>')

              var rows = [];

              // don't display empty rows
              var is_empty = true;

              // append data to row
              _.each(template_form_fields, function (field, index) {
                var str = row[index] || '';
                if (str.length) {
                  is_empty = false;
                }
                rows.push(str);
              });

              if (is_empty) {
                i--;
              } else {
                _.each(rows, function (row) {
                  preview_table.find('tbody > tr:last').append('<td>' + row + '</td>');
                });
              }
            }
          }

          // call fileReader to start reading the file
          fileRead.readAsText(this.form.getValue('file'));
        }
      },
      'click #start-import': function (e) {
        e.preventDefault();

        // 'Infot laetakse serverisse. Palun brauseri-akent mitte sulgeda ega liikuda teisele lehele.'
        Backbone.Message.send_info(T('import-please-wait'));

        // disable start-import button
        this.$el.find('#start-import').attr('disabled', 'disabled');

        // display progressbar
        this.$el.find('.progress').show();
        this.$el.find('.progress').find('.bar').css('width', '33' + '%');

        var self = this;
        var templateId = this.form.getValue('template');

        var uploadComplete = function () {
          if (this.status != 200) {
            // 'Tekkis viga: tõenäoliselt sisaldab fail juba andmebaasis olevaid andmeid'
            Backbone.Message.send_error(T('import-error-duplicated-field'));
          } else {
            // notify/annoy user for successfull upload
            $(document).find('#player')[0].play();

            var templateModel = self.templateCollection.get(templateId);

            // Faili import oli edukas. Andmed talletatud tabelis:
            Backbone.Message.send_success(T('import-success') + templateModel.get('label'))
          }

          // hide progress
          self.$el.find('.progress').hide();

          // enable and hide start-import button
          self.$el.find('#start-import').removeAttr('disabled');
          self.$el.find('#start-import').hide();

          // clean forms
          self.form.setValue({ template: '', file: '' });
        }

        var xhr = new XMLHttpRequest();
        var fd = new FormData(document.getElementById('import-form'));

        fd.append('skip_first_row', this.$el.find('#skip-first-row').is(':checked'));

        /* event listners */
        xhr.addEventListener('load', uploadComplete, false);
        /* Be sure to change the url below to the url of your upload server side script */
        xhr.open('post', '/api/import/' + templateId);

        xhr.setRequestHeader("X-Tara-Grant", Backbone.User.get_grant());

        xhr.send(fd);
      }
    },

    initialize: function (options) {
      this.templateCollection = options.templateCollection;
      this.params = options.params;
    },

    render: function () {
      this.$el.append(this.template);

      // hide start-import button
      this.$el.find('#start-import').hide();

      // hide progress-bar
      this.$el.find('.progress').hide();

      var child_by_parent = {};
      _.each(this.templateCollection.toJSON(), function(item) {
        if (!child_by_parent[item.parent]) {
          child_by_parent[item.parent] = [];
        }
        child_by_parent[item.parent].push(item);
      });

      var form_options = [{val: '', label: '--'}];
      _.each(child_by_parent[null], function(item0) {
        form_options.push({val: item0._id, label: item0.label});
        _.each(child_by_parent[item0._id], function(item1) {
          form_options.push({val: item1._id, label: '-- '+item1.label});
        });
      });

      this.form = new Backbone.Form({
        template: _.template(importFormTmpl),
        schema: {
          template: {
            type: 'Select',
            title: T('import-choose-template'),
            options: form_options,
            validators: ['required']
          },
          file: {
            title: T('import-choose-file'),
            type: InputFileEditor,
            validators: ['required']
          }
        }
      });

      this.$el.find('#form-content').append(this.form.render().el);

      // select correct template if provided
      if(this.params && this.params.template_id) {
        this.$el.find('select').val(this.params.template_id);
      }

      return this;
    }
  });

  return ImportView;
});
