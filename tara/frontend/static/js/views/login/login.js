define(['backbone', 'views/login/form', 'text!/static/templates/login/login.html', 'translator'], function (Backbone, LoginForm, loginTmpl, T) {
  "use strict";

  var LoginView = Backbone.View.extend({
    redirect: '',

    template: _.template(loginTmpl),

    events: {
      'submit .form-signin': function (e) {
        var self = this;

        e.preventDefault();
        var username = e.target[0].value;
        var password = e.target[1].value;

        Backbone.User.login(username, password, function (res) {
          if (!res.error) {
        	  var interval = setInterval( function() {
      	    	if(localStorage.user){
      	    		clearInterval(interval);
      	    		self.options.mainLayout.initialize({templateCollection: self.options.tplCol});
      				  Backbone.history.navigate(self.redirect, {trigger: true});
      		          Backbone.Message.send_success(T('login-success'));
      			  }
      	    }, 250 );
          } else {
            Backbone.Message.send_error(res.error);
          }
        });
      },

      'click #anonymous-login': function (e) {
        var self = this;
        e.preventDefault();

        Backbone.User.login('anonymous', 'anonymous', function (res) {
          if (!res.error) {
            var interval = setInterval( function() {
      	    	if(localStorage.user){
      	    		clearInterval(interval);
      	    		self.options.mainLayout.initialize({templateCollection: self.options.tplCol});
      				  Backbone.history.navigate(self.redirect, {trigger: true});
      		          Backbone.Message.send_success(T('login-success'));
      			  }
      	    }, 250 );
          } else {
            Backbone.Message.send_error(res.error);
          }
        });
      }
    },

    initialize: function (options) {
      if (options.params && options.params.redirect) {
        this.redirect = options.params.redirect;
        this.mainLayout=options.mainLayout;
    	this.tplCol=options.tplCol;
      }
      this.form = new LoginForm();
    },

    render: function () {
      this.$el.append(this.template);
      this.$el.find('#form-content').append(this.form.render().el)
      return this;
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return LoginView;
});
