define(['backbone'], function (Backbone) {
  "use strict";

  var LogoutView = Backbone.View.extend({
    render: function () {
      Backbone.User.logout();
      Backbone.history.navigate('login', {trigger: true});
      return this;
    }
  });

  return LogoutView;
});
