define(['helpers/textarea-json', 'helpers/select-null', 'translator', 'backbone-forms'], function (TextAreaJSONEditor, SelectNullEditor, T) {
  "use strict";

  var UserForm = Backbone.Form.extend({
    initialize: function (options) {

      var rolesOptions = [{val: '', label: '--'}];
      _.each(options.roles.toJSON(), function (role) {
        rolesOptions.push({
          val: role['_id'],
          label: role['name']
        });
      });

      options.schema = {
        username: {
          title: T('user-username'),
          type: 'Text',
          validators: ['required']
        },
        serial: {
          title: T('user-serial'),
          type: 'Text'
        },
        first_name: {
          title: T('user-fname'),
          validators: ['required']
        },
        last_name: {
          title: T('user-lname'),
          validators: ['required']
        },
        email: {
          title: T('user-email'),
          type: 'Text',
          dataType: 'email',
          validators: ['required', 'email']
        },
        password: {
          title: T('user-password'),
          type: 'Password'
        },
        role: {
          title: T('user-role'),
          type: SelectNullEditor,
          options: rolesOptions,
          validators: ['required']
        },
        is_active: {
          title: T('user-is-active'),
          type: 'Checkbox'
        }
      };

      Backbone.Form.prototype.initialize.call(this, options);
    }
  });

  return UserForm;
});
