define(['backbone', 'views/user/form', 'models/user', 'collections/role', 'text!/static/templates/user/edit.html', 'translator'], function (Backbone, UserForm, UserModel, RolesCollection, userEditTmpl, T) {
  "use strict";

  var UserEditView = Backbone.View.extend({
    template: _.template(userEditTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      },
      'click #delete-item': function (e) {
        e.preventDefault();
        if(confirm(T('confirm-delete'))) {
          this.model.destroy({success: function () {
            Backbone.history.navigate('users', {trigger: true});
          }});
        }
      }
    },

    initialize: function (options) {
      this.model = new UserModel({_id: options.userId});
      this.roles = new RolesCollection();
      this.listenTo(this.model, 'sync', this.redirect);
    },

    render: function () {
      var self = this;
      this.$el.append(this.template({permissions: Backbone.View.permissions()}));

      this.roles.fetch({success: function (roles) {

        self.model.fetch({save: false, success: function () {
          var tmpl = _.template('<div data-fields="*"></div>');
          self.form = new UserForm({template: tmpl, model: self.model, roles: roles});
          self.$el.find('#form-content').append(self.form.render().el);
        }});

      }});

      return this;
    },

    redirect: function (model, resp, options) {
      if (options.patch) {
        Backbone.history.navigate('users', {trigger: true});
      }
    },

    onClose: function () {
      if (this.form) {
        this.form.remove();
      }
    }
  });

  return UserEditView;
});
