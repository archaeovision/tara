define(['backgrid', 'translator'], function (Backgrid, T) {
  "use strict";

  var UserGrid = Backgrid.Grid.extend({
    initialize: function (options) {
      options.columns = [
        {
          name: 'username',
          label: T('user-username'),
          editable: false,
          cell: 'string'
        },
        {
          name: 'serial',
          label: T('user-serial'),
          editable: false,
          cell: 'string'
        },
        {
          name: 'email',
          label: T('user-email'),
          editable: true,
          cell: 'string'
        },
        {
          name: 'is_active',
          label: T('user-is-active'),
          editable: true,
          cell: 'boolean'
        },
        {
          name: '_action',
          label: '',
          editable: false,
          cell: Backgrid.Cell.extend({
            render: function () {
              var tmpl = '<a href="/users/<%= _id %>" class="btn">'+ T('button-edit') +'</a>';
              this.$el.html(_.template(tmpl, this.model.attributes));
              return this;
            }
          })
        }
      ];

      this.constructor.__super__.initialize.apply(this, [options]);
    }
  });

  return UserGrid;
});
