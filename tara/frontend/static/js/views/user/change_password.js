define(['backbone', 'models/user', 'text!/static/templates/user/change_password.html', 'translator'], function (Backbone, UserModel, changePasswordTmpl, T) {
  "use strict";

  var ChangePasswordView = Backbone.View.extend({
    template: _.template(changePasswordTmpl),

    events: {
      'submit form': function (e) {
        e.preventDefault();

        var old_pw = this.$el.find('#oldPassword').val();
        var new_pw = this.$el.find('#newPassword').val();
        var new_pw2 = this.$el.find('#newPassword2').val();

        if (!old_pw || !new_pw || !new_pw2) {
          // 'Kõik välja peavad olema täidetud!'
          Backbone.Message.send_error(T('missing-fields'));
          return;
        }

        if (new_pw != new_pw2) {
          // 'Uuesti sisestatud parool oli erinev, proovi uuesti.'
          Backbone.Message.send_error(T('password-mismatch'));
          this.$el.find('inputPassword').val('');
          this.$el.find('inputPassword2').val('');
          return;
        }

        $.post('/api/users/change_password', {'old_password': old_pw, 'new_password': new_pw},
          function () {
            Backbone.Message.send_success(T('change-password-success'));
          }
        ).fail(function () {
          Backbone.Message.send_error(T('change-password-error'));
        });
      }
    },

    render: function () {
      this.$el.append(this.template)
      return this;
    }
  });

  return ChangePasswordView;
});
