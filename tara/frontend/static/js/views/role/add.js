define(['backbone', 'views/role/form', 'models/role', 'text!/static/templates/role/add.html'], function (Backbone, RoleForm, RoleModel, roleAddTmpl) {
  "use strict";

  var RoleAddView = Backbone.View.extend({
    template: _.template(roleAddTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      }
    },

    initialize: function () {
      var model = new RoleModel();
      this.listenTo(model, 'sync', this.redirect);
      this.form = new RoleForm({template: this.template, model: model});
    },

    render: function () {
      this.$el.html(this.form.render().el);
      return this;
    },

    redirect: function () {
      Backbone.history.navigate('roles', {trigger: true});
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return RoleAddView;
});
