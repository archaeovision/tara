define(['collections/role', 'views/role/grid', 'text!/static/templates/role/list.html'], function (RoleCollection, RoleGrid, roleListTmpl) {
  "use strict";

  var RoleListView = Backbone.View.extend({
    template: _.template(roleListTmpl),

    initialize: function () {
      this.collection = new RoleCollection();
      this.grid = new RoleGrid({collection: this.collection});
    },

    render: function () {
      this.$el.append(this.template({permissions: Backbone.View.permissions()}));
      this.$el.find('#grid-content').append(this.grid.render().$el);
      this.collection.fetch({reset: true});
      return this;
    },

    onClose: function () {
      this.grid.remove();
    }
  });

  return RoleListView;
});
