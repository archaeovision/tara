from os import environ
from flask import Flask


def create_app(package_name, settings_override=None, **kwargs):
    """Returns a :class:`Flask` application instance.

    :param package_name: application package name
    :param package_path: application package path
    :param settings_override: a dictionary of settings to override
    """
    app = Flask(package_name, instance_relative_config=True, **kwargs)

    # production
    if 'TARA_PRODUCTION' in environ:
        app.config.from_object('tara.settings.ProductionConfig')
        app.config['MONGO_URI'] = environ['TARA_MONGO_URI']

    # testing
    elif settings_override and settings_override.get('TESTING') is True:
        app.config.from_object('tara.settings.TestingConfig')

    # development
    else:
        app.config.from_object('tara.settings.DevelopmentConfig')

    # override settings
    app.config.from_object(settings_override)

    return app
