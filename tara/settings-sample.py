class BaseConfig(object):
    DEBUG = True
    SSL = True
    THEME_URI = '/static/themes/_THEMENAME_/'
    SYSTEM_NAME = '_THE_DATABASE_'
    HTML_TITLE = '_NAME_'
    LOGO_URI = '/static/themes/_THEMENAME_/img/logo.png'
    LOAD_STYLESHEETS=['https://fonts.googleapis.com/css?family=Open+Sans:400,300','bootstrap.css','backgrid.css','backgrid-paginator.css','default.css','custom.css','forms.css','print.css']
    ROOT_DIRECTORY='/opt/www/_ROOTDIR_'
    FAVICON_URI = '/static/themes/_THEMENAME_/img/favicon.ico'

class ProductionConfig(BaseConfig):
    SEND_FILE_MAX_AGE_DEFAULT = 0
    SSL = True


class DevelopmentConfig(BaseConfig):
    MONGO_URI = 'mongodb://localhost:27017/_DATABASE_'
    SEND_FILE_MAX_AGE_DEFAULT = 0


class TestingConfig(BaseConfig):
    TESTING = True
    MONGO_URI = 'mongodb://localhost:27017/_DATABASE_TEST_'
