import json
from bson import objectid
from pymongo import cursor
import datetime
import calendar


class MongoJsonEncoder(json.JSONEncoder):
    def default(self, obj, **kwargs):
        if isinstance(obj, objectid.ObjectId):
            return str(obj)
        elif isinstance(obj, cursor.Cursor):
            return map(None, obj)
        elif isinstance(obj, datetime.datetime):
            if obj.utcoffset() is not None:
                obj = obj - obj.utcoffset()
            millis = int(
                calendar.timegm(obj.timetuple()) * 1000 +
                obj.microsecond / 1000
            )
            return millis
        return json.JSONEncoder.default(obj, **kwargs)
