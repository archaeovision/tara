import re

from functools import wraps
from flask import jsonify, request, current_app
from tara.api.service.exceptions import ResourceNotFound

from tara.restful_login import current_user
from tara.core import mongo

from bson.objectid import ObjectId

from datetime import datetime, date, timedelta

def login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_app.config['TESTING']:
            return func(*args, **kwargs)

        if not current_user:
            return current_app.login_manager.unauthenticated()

        permissions = current_app.mongo.db.roles.find_one(current_user['role'])['permissions']
        allowed_methods = []

        # Add permission to request own user info
        permissions.append({
            'url': '/users/'+str(current_user['_id']),
            'methods': ['GET']
        })

        # Add permission to change password
        if current_user['username'] != 'anonymous':
            permissions.append({
                'url': '/users/change_password',
                'methods': ['POST']
            })

        # Add permission to request own role info
        permissions.append({
            'url': '/roles/'+str(current_user['role']),
            'methods': ['GET']
        })

        # Add permissions to files
        permissions.append({
            'url': '/files',
            'methods': ['GET']
        })

        # Add permissions to classificators
        permissions.append({
            'url': '/classificators',
            'methods': ['GET']
        })
        fv={};
        for rule in permissions:
            if re.match(rule['url'], request.path):
                allowed_methods = allowed_methods + rule['methods']

        # User role has special forced variables for additional permissions
                #if 'force_vars' in rule, append those to args:
                fv=rule.get('force_vars',{'none':'none'})
                if request.method in fv:
                    kwargs['force_value']=fv[request.method]
                    
        if request.method in allowed_methods:
            return func(*args, **kwargs)

        # file DELETE specialcase, file owner may delete file
        if request.method == 'DELETE':
            splitted = request.path.split('/')
            if len(splitted) == 3 and splitted[1] == 'files':
                user_file = mongo.db.fs.files.find_one(ObjectId(splitted[2]))
                if not user_file:
                    raise ResourceNotFound
                if user_file['_creator_id'] == current_user['_id']:
                    return func(*args, **kwargs)

        return current_app.login_manager.unauthorized()
    return decorated_view


def return_json(f):
    def decorator(*args, **kwargs):
        data = f(*args, **kwargs)
        if isinstance(data, dict):
            return jsonify(data)
        else:
            return data
    return decorator
def tolastdayinisoweek(year, week):
        year = int(year)
        week = int(week)
        ret = datetime.strptime('%04d-%02d-1' % (year, week), '%Y-%W-%w')
        if date(year, 1, 4).isoweekday() > 4:
            ret -= timedelta(days=1)
        return ret