from flask import Blueprint, request

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.file import FileService

from tara.api.util.decorators import login_required


class FileView(BaseMethodView):
    def __init__(self):
        self.service = FileService()

    @login_required
    def get(self, _id):
        return self.service.find(_id, request.args)

    @login_required
    def post(self):
        for key, value in request.files.iteritems(multi=True):
            _id = self.service.insert(value)
        # returns only last uploaded file id
        return self.service.find(_id)


bp = Blueprint('file', __name__, url_prefix='/files')
register_api(bp, FileView)
