from flask import Blueprint, request

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.category import CategoryService


class CategoryView(BaseMethodView):
    def __init__(self):
        self.service = CategoryService()

    def get(self, _id):
        rargs=request.args.copy()
        return self.service.find(_id, rargs)


bp = Blueprint('category', __name__, url_prefix='/categories')
register_api(bp, CategoryView)