from flask import Blueprint

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.role import RoleService


class RoleView(BaseMethodView):
    def __init__(self):
        self.service = RoleService()


bp = Blueprint('role', __name__, url_prefix='/roles')
register_api(bp, RoleView)
