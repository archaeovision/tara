import bson
from flask import Blueprint

from tara.api.controller import BaseMethodView
from tara.api.service.autocomplete import AutocompleteService


class AutocompleteView(BaseMethodView):
    def __init__(self):
        self.service = AutocompleteService()

    def get(self, find_by, field):
        if bson.objectid.ObjectId.is_valid(find_by):
            return self.service.find(bson.objectid.ObjectId(find_by), field)
        else:
            return self.service.find_by_collection(find_by, field)


bp = Blueprint('autocomplete', __name__, url_prefix='/autocomplete')

view_func = AutocompleteView.as_view(bp.name)
view_func.provide_automatic_options = False

bp.add_url_rule('/<find_by>/<field>', view_func=view_func, methods=['GET'])