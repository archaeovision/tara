from flask import Blueprint, jsonify, request
from tara.restful_login import current_user
from tara.api.service.auth import AuthService

bp = Blueprint('oauth', __name__, url_prefix='/auth')

service = AuthService()


# Every authentication call needs to include request token.
# This endpoint returns temporary authentication token.
@bp.route('/request_token')
def request_token():
    token = service.request_token()
    return jsonify(dict(token=token))


# Authentication
@bp.route('/authenticate', methods=['POST'])
def authenticate():
    token = request.headers.get('X-Tara-Token')
    username = request.form.get('username')
    password = request.form.get('password')
    resp = service.authenticate(token, username, password)
    return jsonify(resp)


@bp.route('/logout')
def logout():
    if current_user:
        service.logout(current_user['_id'])
        return jsonify(status='User logged out')
    return jsonify(error='Cannot logout not authenticated user')
