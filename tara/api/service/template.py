import bson

from schema import Schema, And, Or, Use, Optional

from tara.core import mongo
from tara.api.service.exceptions import ResourceNotFound

LABEL_SCHEMA = And(basestring, len)
SORT_SCHEMA = Or(None, int)
PARENT_SCHEMA = Or(None, And(basestring, lambda s: bson.ObjectId.is_valid(s), Use(lambda s: bson.ObjectId(s))))
LAYOUT_SCHEMA = PARENT_SCHEMA
UNIQUE_SCHEMA = [basestring]
SHOW_ALL_BUTTON_ENABLED = bool
OPT_TEXT_SCHEMA = Or(None, basestring)

GRID_FIELD_TYPES = ['string','text','number','uri','array_separator','delimited','date']
GRID_FIELDS_SCHEMA = [{
    'name': basestring,
    'label': basestring,
    'type': And(basestring, lambda s: s in GRID_FIELD_TYPES),
    Optional('collection'): basestring,
    Optional('joinedby'): basestring,
    Optional('joinedto'): basestring,
    Optional('linksto'): basestring,
    Optional('linkstoid'): basestring,
    Optional('linkedname'): basestring,
    Optional('linkedresource'): basestring,
    Optional('separator'): basestring,
    Optional('external_sorter'): basestring,
    Optional('default_sort'): basestring,
    Optional('default_order'): basestring
}]

FORM_FIELD_TYPES = ['array', 'array_separator', 'date', 'delimited', 'text', 'string','textarea', 'number', 'metadata', 'classificator', 'classificator_multi','password', 'uri']
FORM_FIELDS_SCHEMA = [{
    'name': basestring,
    'label': basestring,
    'type': And(basestring, lambda s: s in FORM_FIELD_TYPES),
    Optional('autocomplete'): {'template_id': basestring, 'field': basestring},
    Optional('required'): bool,
    Optional('where'): basestring,
    Optional('separator'): basestring,
    Optional('collection'): basestring,
    Optional('joinedby'): basestring,
    Optional('joinedto'): basestring,
    Optional('linksto'): basestring,
    Optional('linkstoid'): basestring,
    Optional('linkedname'): basestring,
    Optional('readonly'): bool,
    Optional('linkedresource'): basestring,
    Optional('separator'): basestring,
    Optional('external_sorter'): basestring
}]


class TemplateService():
    def __init__(self):
        self.collection = mongo.db.templates

    def find(self, _id=None):
        if _id is None:
            return dict(result=self.collection.find().sort('sort'))
        else:
            result = self.collection.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, data):
        validator = {
            'label': LABEL_SCHEMA,
            'grid_fields': GRID_FIELDS_SCHEMA,
            'form_fields': FORM_FIELDS_SCHEMA,
            Optional('show_all_enabled'): SHOW_ALL_BUTTON_ENABLED,
            Optional('parent'): PARENT_SCHEMA,
            Optional('layout'): LAYOUT_SCHEMA,
            Optional('collection'): OPT_TEXT_SCHEMA,
#            Optional('sublist_of'): UNIQUE_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
            Optional('unique'): UNIQUE_SCHEMA
        }
        
        document = Schema(validator).validate(data)
        return self.collection.insert(document)

    def update(self, _id, data):
        validator = {
            Optional('label'): LABEL_SCHEMA,
            Optional('collection'): OPT_TEXT_SCHEMA,
#            Optional('sublist_of'): UNIQUE_SCHEMA,
            Optional('grid_fields'): GRID_FIELDS_SCHEMA,
            Optional('form_fields'): FORM_FIELDS_SCHEMA,
            Optional('show_all_enabled'): SHOW_ALL_BUTTON_ENABLED,
            Optional('parent'): PARENT_SCHEMA,
            Optional('layout'): LAYOUT_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
            Optional('unique'): UNIQUE_SCHEMA
        }

        document = Schema(validator).validate(data)
        self.collection.update({'_id': _id}, {'$set': document})

    def remove(self, _id):
        self.collection.remove(_id)
