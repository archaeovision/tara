from schema import Schema, And, Or, Optional

from tara.core import mongo
from tara.api.service.exceptions import ResourceNotFound

LABEL_SCHEMA = And(basestring, len)
SORT_SCHEMA = Or(None, int)


WIDGETS_SCHEMA = [{
    'path': basestring,
    Optional('config'): {
        basestring: object
    }
}]


class TemplateLayoutService():
    def __init__(self):
        self.collection = mongo.db.template_layouts

    def find(self, _id=None):
        if _id is None:
            return dict(result=self.collection.find().sort('sort'))
        else:
            result = self.collection.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, data):
        validator = {
            'label': LABEL_SCHEMA,
            'widgets': WIDGETS_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
        }

        document = Schema(validator).validate(data)
        return self.collection.insert(document)

    def update(self, _id, data):
        validator = {
            Optional('label'): LABEL_SCHEMA,
            Optional('widgets'): WIDGETS_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
        }

        document = Schema(validator).validate(data)
        self.collection.update({'_id': _id}, {'$set': document})

    def remove(self, _id):
        self.collection.remove(_id)
