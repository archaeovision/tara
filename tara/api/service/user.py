from bson.objectid import ObjectId

from schema import Schema, And, Use, Optional
from tara.core import mongo, bcrypt
from tara.api.service.exceptions import ResourceNotFound, OldPasswordMismatch

DISPLAY_FIELDS = dict(fields=['username', 'serial', 'first_name', 'last_name', 'email', 'role', 'is_active'])

USERNAME_SCHEMA = And(basestring, len, lambda s: mongo.db.users.find(dict(username=s)).count() == 0)
PASSWORD_SCHEMA = And(basestring, len, Use(bcrypt.generate_password_hash))
SERIAL_SCHEMA = And(basestring, len)
FIRSTNAME_SCHEMA = And(basestring, len)
LASTNAME_SCHEMA = And(basestring, len)
EMAIL_SCHEMA = And(basestring, len)
ROLE_SCHEMA = And(basestring, len, Use(ObjectId), lambda role: mongo.db.roles.find_one(role))
IS_ACTIVE_SCHEMA = And(bool)


class UserService():
    def __init__(self):
        self.users = mongo.db.users

    def find(self, _id=None):
        if _id is None:
            return dict(result=self.users.find(**DISPLAY_FIELDS))
        else:
            result = self.users.find_one(_id, **DISPLAY_FIELDS)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, data):
        schema = Schema(dict(
            username=USERNAME_SCHEMA,
            password=PASSWORD_SCHEMA,
            serial=Optional(SERIAL_SCHEMA),
            first_name=FIRSTNAME_SCHEMA,
            last_name=LASTNAME_SCHEMA,
            email=EMAIL_SCHEMA,
            role=ROLE_SCHEMA,
            is_active=IS_ACTIVE_SCHEMA
        ))
        document = schema.validate(data)
        return self.users.insert(document)

    def update(self, _id, data):
        if 'username' in data and self.find(_id)['username'] != data['username']:
            Schema(USERNAME_SCHEMA).validate(data['username'])

        schema = Schema({
            Optional('username'): basestring,
            Optional('password'): PASSWORD_SCHEMA,
            Optional('serial'): SERIAL_SCHEMA,
            Optional('first_name'): FIRSTNAME_SCHEMA,
            Optional('last_name'): LASTNAME_SCHEMA,
            Optional('email'): EMAIL_SCHEMA,
            Optional('role'): ROLE_SCHEMA,
            Optional('is_active'): IS_ACTIVE_SCHEMA
        })
        document = schema.validate(data)
        self.users.update(dict(_id=_id), {'$set': document})

    def remove(self, _id):
        self.users.remove(_id)

    def change_password(self, user_id, old_password, new_password):
        user = self.users.find_one(user_id)

        if not bcrypt.check_password_hash(user['password'], old_password):
            raise OldPasswordMismatch()

        new_pw_hash = bcrypt.generate_password_hash(new_password)
        self.users.update(dict(_id=user_id), {'$set': {'password': new_pw_hash}})
