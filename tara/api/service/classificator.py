from datetime import datetime
from schema import Schema, And, Or, Optional
from tara.core import mongo
from tara.restful_login import current_user
from tara.api.service.exceptions import ResourceNotFound

NAME_SCHEMA = And(basestring, len)
VALUE_SCHEMA = And(basestring, len)
TYPE_SCHEMA = And(basestring, len)
SORT_SCHEMA = Or(None, int)
DESC_SCHEMA = basestring


class ClassificatorService():
    def find(self, _id, args=None):
        if _id is None:
            query = {}
            type_ = args.get('type', type=str)
            if type_:
                query = {'type': type_}

            # cursor
            cursor = mongo.db.classificators.find(query)

            # sorting
            if args.get('sort_by'):
                sort_by = args.get('sort_by', type=str)
                order = args.get('order', type=int)
                cursor.sort(sort_by, order)

            # skipping
            if args.get('page'):
                page = args.get('page', type=int)
                per_page = args.get('per_page', type=int)
                cursor.skip((page-1) * per_page).limit(per_page)

            # return result
            return dict(result=cursor, total_entries=cursor.count())
        else:
            result = mongo.db.classificators.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, data):
        document = Schema({
            'name': NAME_SCHEMA,
            'value': VALUE_SCHEMA,
            'type': TYPE_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
            Optional('description'): DESC_SCHEMA
        }).validate(data)

        if current_user:
            document['_creator_id'] = current_user['_id']
            document['_creator_name'] = current_user['full_name']

        document['_inserted'] = datetime.utcnow()

        return mongo.db.classificators.insert(document)

    def update(self, _id, data):
        document = Schema({
            Optional('name'): NAME_SCHEMA,
            Optional('value'): VALUE_SCHEMA,
            Optional('type'): VALUE_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
            Optional('description'): DESC_SCHEMA
        }).validate(data)

        if current_user:
            document['_editor_id'] = current_user['_id']
            document['_editor_name'] = current_user['full_name']

        document['_updated'] = datetime.utcnow()

        mongo.db.classificators.update(dict(_id=_id), {'$set': document})

    def remove(self, _id):
        mongo.db.classificators.remove(_id)
