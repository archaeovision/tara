from datetime import datetime
from schema import Schema, And, Or, Optional
from tara.core import mongo
from tara.restful_login import current_user
from tara.api.service.exceptions import ResourceNotFound

NAME_SCHEMA = And(basestring, len)
URL_SCHEMA = Or(None, basestring)
IS_EXT_SCHEMA = bool
ICON_SCHEMA = And(basestring)
SORT_SCHEMA = Or(None, int)
PARENT_SCHEMA = Or(basestring, len, None)

class CategoryService():
    def __init__(self):
        self.categories = mongo.db.categories

    def find(self, _id=None, data=[]):
        if _id is None:
            if not 'menu' in data:
                return dict(result=self.categories.find().sort('sort'))
            
            data=self.categories.find().sort('sort')
            
            nres=[]
            children=[]
            res=[]
            for row in data:
                row['children']=[]
                res.append(row)
                if 'parent' in row and len(row['parent'].strip()):
                    children.append(row)
                else:
                    nres.append(row)
            # If there are no child menus, just return all the menu
            if len(children) == 0:
                return dict(result=res)
            #Render all the parents
            for row in nres:
                #Render all the children
                for child in children:
                    if len(child['parent'].strip()) and child['parent'] in row['name']:
                        row['children'].append(child)
                if len(row['children']):
                    for child in row['children']:
                        child = self.get_all_children(child, children)
            return dict(result=nres)
        else:
            result = self.categories.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            return result
        
    def get_all_children(self, menuitem, children):
        for child in children:
            if len(child['parent'].strip()) and child['parent'] in menuitem['name']:
                menuitem['children'].append(child)
        if len(menuitem['children']):
            for child in menuitem['children']:
                child = self.get_all_children(child, children) 
        return menuitem

    def insert(self, data):
        document = Schema({
            'name': NAME_SCHEMA,
            Optional('url'): URL_SCHEMA,
            'is_external': IS_EXT_SCHEMA,
            Optional('icon'): ICON_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
            Optional('parent'): PARENT_SCHEMA
        }).validate(data)

        if current_user:
            document['_creator_id'] = current_user['_id']
            document['_creator_name'] = current_user['full_name']

        document['_inserted'] = datetime.utcnow()

        return self.categories.insert(document)

    def update(self, _id, data):
        document = Schema({
            Optional('name'): NAME_SCHEMA,
            Optional('url'): URL_SCHEMA,
            Optional('is_external'): IS_EXT_SCHEMA,
            Optional('icon'): ICON_SCHEMA,
            Optional('sort'): SORT_SCHEMA,
            Optional('parent'): PARENT_SCHEMA
        }).validate(data)

        if current_user:
            document['_editor_id'] = current_user['_id']
            document['_editor_name'] = current_user['full_name']

        document['_updated'] = datetime.utcnow()

        self.categories.update(dict(_id=_id), {'$set': document})

    def remove(self, _id):
        self.categories.remove(_id)
