# -*- coding: utf-8 -*-
import re, bson

from datetime import date, datetime
import calendar

from datetime import datetime

from schema import Schema, Optional, And, Or, Use
from tara.core import mongo
from tara.restful_login import current_user
from tara.api.service.revision import RevisionService
from tara.api.service.exceptions import ResourceNotFound, SearchQueryError, InsertEmptyRecordError

RESERVED_ARGS = ['page', 'per_page', 'sort_by', 'order', 'total_pages', 'total_entries']


class ResourceService():
    def __init__(self):
        self.revision_service = RevisionService()

    def collection(self, template_id):
        template = mongo.db.templates.find_one(template_id)

        if template is None:
            raise ResourceNotFound()

        if 'collection' in template:
            coll = str(template['collection'] or template['parent'] or template['_id'])
        else:
            coll = str(template['parent'] or template['_id'])
        return [mongo.db[coll], template]

    def find(self, template_id, _id, args):
        collection, template = self.collection(template_id)
        
        if _id is None:
            query = {'$and': []}
            # names list for later outputting only values from configuration
            tnames=[(rec['name']) for rec in template['grid_fields']]
            tnames.extend(['_creator_id','_creator_name','_id','_inserted','_template_id'])
            try:
                for key, values in args.iterlists():
                    if key not in RESERVED_ARGS:
                        if len(key) > 2 and key[-2:] == '[]':
                            key = key[:-2]
                        if len(values) == 1 and len(values[0]):
                            if key == '_ALL_':
                                _or = []
                                for item in template['form_fields']:
                                    if item['type'] in ['text', 'textarea']:
                                        _or.append({item['name']: {'$regex': re.compile(values[0], re.IGNORECASE)}})
                                    elif item['type'] == 'number' and values[0].isdigit():
                                        _or.append({item['name']: int(values[0])})
                                query['$and'].append({'$or': _or})
                            else:
                                item = filter(lambda s: s['name'] == key, template['form_fields'])[0]
                                if item['type'] in ['text', 'textarea']:
                                    val = re.compile(values[0], re.IGNORECASE)
                                    query[key] = {'$regex': re.compile(values[0], re.IGNORECASE)}
                                elif item['type'] in 'delimited':
                                    val = values[0].replace('$','').replace('^','')
                                    query[key] = {'$regex': val}
                                elif item['type'] in 'date':
                                    if '<' in values[0] or '>' in values[0]:
                                        dstr=values[0].replace('>','').strip()
                                        dstr=dstr.replace('<','').strip()
                                        try:
                                            date=datetime.strptime(dstr, '%d.%m.%Y')
                                        except ValueError:
                                            try:
                                                date=datetime.strptime(dstr, '%Y-%m-%d')
                                            except ValueError:
                                                pass
                                        if date:
                                            datestr=date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
                                            if '<' in values[0]:
                                                query[key] = {'$lt': datestr}
                                            else:
                                                query[key] = {'$gt': datestr}
                                elif item['type'] == 'number' and values[0].isdigit():
                                    query[key] = int(values[0])
                                elif item['type'] == 'number' and '>' in values[0]:
                                    query[key] = {'$gt': int(values[0].replace('>','').strip())}
                                elif item['type'] == 'number' and '<' in values[0]:
                                    query[key] = {'$lt': int(values[0].replace('<','').strip())}

                        elif len(values) > 1:
                            if key == '_ALL_':
                                _or = []
                                for val in values:
                                    for item in template['form_fields']:
                                        if item['type'] in ['text', 'textarea']:
                                            _or.append({item['name']: {'$regex': re.compile(val, re.IGNORECASE)}})
                                        elif item['type'] == 'number' and val.isdigit():
                                            _or.append({item['name']: int(val)})
                                query['$and'].append({'$or': _or})
                            else:
                                item = filter(lambda s: s['name'] == key, template['form_fields'])[0]
                                if item['type'] in ['text', 'textarea']:
                                    res = map(lambda xval: {key: {'$regex': re.compile(xval, re.IGNORECASE)}}, values)
                                    query['$and'].append({'$or': res})
                                elif item['type'] == 'number':
                                    res = map(lambda xval: {key: int(xval)}, filter(lambda x: x.isdigit(), values))
                                    query['$and'].append({'$or': res})
                              

            except re.error:
                raise SearchQueryError

            if len(query['$and']) == 0:
                del query['$and']

            if template['parent']:
                query['_template_id'] = template_id

            # filter out deleted records
            query['_deleted'] = {'$exists': False}

            # cursor
            cursor = collection.find(query)

            # sorting
            if args.get('sort_by'):
                sort_by = args.get('sort_by',type=None)
                for item in template['form_fields']:
                    if item['name'] == sort_by and 'external_sorter' in item:
                        sort_by=item['external_sorter']
                order = args.get('order', type=int)
                cursor.sort(sort_by, order)

            # skipping
            if args.get('page'):
                page = args.get('page', type=int)
                per_page = args.get('per_page', type=int)
                cursor.skip((page-1) * per_page).limit(per_page)

            # return result
            res=[]
            for result in cursor:
                r2={}
                for k,v in result.iteritems():
                    if k in tnames:
                        r2[k]=v
                res.append(r2)
            res=dict(result=res, total_entries=cursor.count())
            # do we have any joined fields here? query collections
            # ask separate queries from mongo and insert data to res(ult) dictionary
            #res['result']=dict(res['result'])
            
            for item in template['grid_fields']:
                #res['result'][0][item['name']]=str(item)
                if 'collection' in item and 'joinedby' in item:
                    l=[]
                    # ask data about all fields that contain joins
                    for k in res['result']:
                        l.append(k[item['joinedby']])
                    
                    if not 'joinedto' in item:
                        item['joinedto']=item['joinedby']
                    if not 'linkedname' in item:
                        item['linkedname']=item['name']
                    joinfields=mongo.db[item['collection']].find({item['joinedto']:{"$in":l}},{"_id":1,item['linkedname']:1,item['joinedto']:1})
                    joiner={}
                    idjoiner={}
                    for record in joinfields:
                        joiner[record[item['joinedto']]]=record[item['linkedname']]
                        idjoiner[record[item['joinedto']]]=record['_id']
                    for k, v in enumerate(res['result']):
                        res['result'][k][item['name']]=str(joiner[v[item['joinedto']]])
                        #shall we create a link
                        if 'linksto' in item:
                            href='/resources/'+item['linksto']+'/'+str(idjoiner[v[item['joinedto']]])
                            res['result'][k][item['name']]='<a href="'+href+'">'+str(res['result'][k][item['name']])+'</a>'
                elif 'linkedresource' in item and 'separator' in item and 'delimited' in item['type']:
                    try:
                        l_items = item['linkedresource'].split( '.' )
                        if len(l_items) is not 2:
                            continue
                        cl=l_items[0]
                        field=l_items[1]
                        for k, v in enumerate(res['result']):
                            byLabel=False
                            try:
                                # Get the resource id
                                rsRow = mongo.db['templates'].find_one({'collection':cl})
                                #If resource was not found by collection name, lets try by label
                                if rsRow is None:
                                    rsRow = mongo.db['templates'].find_one({'label':cl})
                                    if rsRow:
                                        byLabel=True
                            except re.error:
                                rsRow=None
                            # If resource ID not found, nothing to do
                            if rsRow is None:
                                continue
                            if item['name'] not in res['result'][k]:
                                continue
                            rfs=res['result'][k][item['name']].split( item['separator'] )
                            links=''
                            for i, val in enumerate(rfs):
                                # Otherwise get the document from selected collection where the linkedfield matches the item's linked field's value
                                try:
                                    if byLabel:
                                        ctdRow = mongo.db[str(rsRow['_id'])].find_one({field:val.strip()})
                                    else:
                                        ctdRow = mongo.db[cl].find_one({field:val.strip()})
                                except re.error:
                                    ctdRow = None
                                
                                if ctdRow is None:
                                    href='/resources/'+str(rsRow['_id'])
                                else:
                                    href='/resources/'+str(rsRow['_id'])+'/'+str(ctdRow['_id'])
                                
                                if len(val):
                                    links+='<a href="'+href+'">'+val+'</a>'+item['separator']
                            res['result'][k][item['name']]=links
                    except ValueError:
                        pass
                              
            return res

        else:
            res={}
            # names list for later outputting only values from configuration
            tnames=[(rec['name']) for rec in template['grid_fields']]
            tnames.extend(['_creator_id','_creator_name','_id','_inserted','_template_id'])
            result = collection.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            for k,v in result.iteritems():
                if k in tnames:
                    res[k]=v
            for item in template['grid_fields']:
                if 'linkedresource' in item and 'separator' in item and 'delimited' in item['type']:
                    try:
                        l_items = item['linkedresource'].split( '.' )
                        if len(l_items) is not 2:
                            continue
                        cl=l_items[0]
                        field=l_items[1]
                        try:
                            byLabel=False
                            # Get the resource id
                            rsRow = mongo.db['templates'].find_one({'collection':cl})
                            #If resource was not found by collection name, lets try by label
                            if rsRow is None:
                                rsRow = mongo.db['templates'].find_one({'label':cl})
                                if rsRow:
                                    byLabel=True
                        except re.error:
                            rsRow=None
                        # If resource ID not found, nothing to do
                        if rsRow is None:
                            continue
                        if item['name'] not in res:
                            continue
                        rfs=res[item['name']].split( item['separator'] )
                        links=''
                        for i, val in enumerate(rfs):
                            if len(val.strip()):
                                # Otherwise get the document from selected collection where the linkedfield matches the item's linked field's value
                                try:
                                    if byLabel:
                                        ctdRow = mongo.db[str(rsRow['_id'])].find_one({field:val.strip()})
                                    else:
                                        ctdRow = mongo.db[cl].find_one({field:val.strip()})
                                except re.error:
                                    ctdRow = None
                                href=''
                                try:
                                    if ctdRow is None:
                                        href='/resources/'+str(rsRow['_id'])
                                    else:
                                        href='/resources/'+str(rsRow['_id'])+'/'+str(ctdRow['_id'])
                                except ValueError:
                                    href=val + item['separator']
                                if len(val):
                                    links+='<a href="'+href+'">'+val+'</a>'+item['separator']
                        res[item['name']]=links
                    except ValueError:
                        pass
            return res

    def insert(self, template_id, data):
        # Don't allow empty data
        if len(data) == 0:
            raise InsertEmptyRecordError

        collection, template = self.collection(template_id)

        schema = {}
        external_sorters = {}

        for item in template['form_fields']:
            required = True if 'required' in item and item['required'] is True else False
            is_text = item['type'] in ['text', 'textarea', 'classificator', 'delimited']
            is_numb = item['type'] == 'number'
            is_date = item['type'] == 'date'

            if 'external_sorter' in item:
                external_sorters[item['name']]=item['external_sorter']

            if is_text or is_date:
                if required:
                    schema[item['name']] = Or(And(basestring, len), And(Or(int, float), Use(str)))
                else:
                    schema[item['name']] = Or(basestring, And(Or(int, float), Use(str)))
            elif is_numb:
                schema[item['name']] = Use(lambda s: int(s) if isinstance(s, basestring) and s.isdigit() else s)
            else:
                if required:
                    schema[item['name']] = And(Or(list, dict), len)
                else:
                    schema[item['name']] = Or(list, dict)

        document = Schema(schema).validate(data)
        is_empty = True

        # trim whitspaces from left and right and check document
        for key in document:
            if isinstance(document[key], basestring):
                document[key] = document[key].strip()
            if isinstance(document[key], int):
                is_empty = False
            else:
                if len(document[key]):
                    is_empty = False

        if is_empty:
            raise InsertEmptyRecordError

        document['_template_id'] = template_id

        # Create external sorting field with int value
        for key in external_sorters:
            if key in document:
                document[external_sorters[key]]=int(re.sub('[^.\-\d]', '', document[key]))

        if current_user:
            document['_creator_id'] = current_user['_id']
            document['_creator_name'] = current_user['full_name']

        document['_inserted'] = datetime.utcnow()

        # insert document
        return collection.insert(document)

    def update(self, template_id, _id, data):
        # Don't allow empty data
        if len(data) == 0:
            raise InsertEmptyRecordError

        collection, template = self.collection(template_id)

        schema = {}
        external_sorters = {}

        for item in template['form_fields']:
            required = True if 'required' in item and item['required'] is True else False
            is_text = item['type'] in ['text', 'textarea', 'classificator', 'delimited']
            is_numb = item['type'] == 'number'
            is_date = item['type'] == 'date'

            if 'external_sorter' in item:
                external_sorters[item['name']]=item['external_sorter']

            if is_text or is_date:
                if required:
                    schema[Optional(item['name'])] = Or(And(basestring, len), And(Or(int, float), Use(str)))
                else:
                    schema[Optional(item['name'])] = Or(basestring, And(Or(int, float), Use(str)))
            elif is_numb:
                schema[Optional(item['name'])] = Use(lambda s: int(s) if isinstance(s, basestring) and s.isdigit() else s)
            else:
                if required:
                    schema[Optional(item['name'])] = And(Or(list, dict), len)
                else:
                    schema[Optional(item['name'])] = Or(list, dict)

        document = Schema(schema).validate(data)

        old_document = self.find(template_id, _id, None)
        filtered = {}

        # store old values to revision and
        # trim whitspaces from left and right in new document
        for key in document:
            filtered[key] = old_document[key] if key in old_document else None
            if isinstance(document[key], basestring):
                document[key] = document[key].strip()

        self.revision_service.insert(dict(template_id=template_id, resource_id=_id, old=filtered, new=document))

        # Create external sorting field with int value
        for key in external_sorters:
            if key in document:
                document[external_sorters[key]] = int(re.sub('[^.\-\d]', '', document[key]))

        # update user information
        if current_user:
            document['_editor_id'] = current_user['_id']
            document['_editor_name'] = current_user['full_name']

        document['_updated'] = datetime.utcnow()
        # update document
        collection.update({'_id': _id}, {'$set': document})

    def remove(self, template_id, _id):
        document = dict(_deleted=datetime.utcnow())

        if current_user:
            document['_deletor_id'] = current_user['_id']
            document['_deletor_name'] = current_user['full_name']

        self.collection(template_id)[0].update({'_id': _id}, {'$set': document})
